################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adc_driver.c \
../src/bt_bridge_thread_entry.c \
../src/bt_bridge_uart_driver.c \
../src/bt_dtm_uart_driver.c \
../src/bt_thread_entry.c \
../src/cli_thread_entry.c \
../src/cli_uart_driver.c \
../src/hw_io_api.c \
../src/input_thread_entry.c \
../src/mac_i2c_driver.c \
../src/output_thread_entry.c \
../src/pre_scheduler_init.c \
../src/pushbutton_driver.c \
../src/pwm_driver.c \
../src/relay_module.c \
../src/rtc_i2c_driver.c \
../src/watchdog_driver.c \
../src/wifi_thread_entry.c \
../src/wifi_uart_driver.c 

OBJS += \
./src/adc_driver.o \
./src/bt_bridge_thread_entry.o \
./src/bt_bridge_uart_driver.o \
./src/bt_dtm_uart_driver.o \
./src/bt_thread_entry.o \
./src/cli_thread_entry.o \
./src/cli_uart_driver.o \
./src/hw_io_api.o \
./src/input_thread_entry.o \
./src/mac_i2c_driver.o \
./src/output_thread_entry.o \
./src/pre_scheduler_init.o \
./src/pushbutton_driver.o \
./src/pwm_driver.o \
./src/relay_module.o \
./src/rtc_i2c_driver.o \
./src/watchdog_driver.o \
./src/wifi_thread_entry.o \
./src/wifi_uart_driver.o 

C_DEPS += \
./src/adc_driver.d \
./src/bt_bridge_thread_entry.d \
./src/bt_bridge_uart_driver.d \
./src/bt_dtm_uart_driver.d \
./src/bt_thread_entry.d \
./src/cli_thread_entry.d \
./src/cli_uart_driver.d \
./src/hw_io_api.d \
./src/input_thread_entry.d \
./src/mac_i2c_driver.d \
./src/output_thread_entry.d \
./src/pre_scheduler_init.d \
./src/pushbutton_driver.d \
./src/pwm_driver.d \
./src/relay_module.d \
./src/rtc_i2c_driver.d \
./src/watchdog_driver.d \
./src/wifi_thread_entry.d \
./src/wifi_uart_driver.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -I"C:\git_hbird\meg_board\synergy_cfg\ssp_cfg\bsp" -I"C:\git_hbird\meg_board\synergy_cfg\ssp_cfg\driver" -I"C:\git_hbird\meg_board\synergy\ssp\inc" -I"C:\git_hbird\meg_board\synergy\ssp\inc\bsp" -I"C:\git_hbird\meg_board\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git_hbird\meg_board\synergy\ssp\inc\driver\api" -I"C:\git_hbird\meg_board\synergy\ssp\inc\driver\instances" -I"C:\git_hbird\meg_board\src" -I"C:\git_hbird\meg_board\src\synergy_gen" -I"C:\git_hbird\meg_board\synergy_cfg\ssp_cfg\framework\el" -I"C:\git_hbird\meg_board\synergy\ssp\inc\framework\el" -I"C:\git_hbird\meg_board\synergy\ssp\src\framework\el\tx" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


