################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/synergy_gen/Input_thread.c \
../src/synergy_gen/bt_bridge_thread.c \
../src/synergy_gen/bt_thread.c \
../src/synergy_gen/cli_thread.c \
../src/synergy_gen/common_data.c \
../src/synergy_gen/hal_data.c \
../src/synergy_gen/main.c \
../src/synergy_gen/output_thread.c \
../src/synergy_gen/pin_data.c \
../src/synergy_gen/wifi_thread.c 

OBJS += \
./src/synergy_gen/Input_thread.o \
./src/synergy_gen/bt_bridge_thread.o \
./src/synergy_gen/bt_thread.o \
./src/synergy_gen/cli_thread.o \
./src/synergy_gen/common_data.o \
./src/synergy_gen/hal_data.o \
./src/synergy_gen/main.o \
./src/synergy_gen/output_thread.o \
./src/synergy_gen/pin_data.o \
./src/synergy_gen/wifi_thread.o 

C_DEPS += \
./src/synergy_gen/Input_thread.d \
./src/synergy_gen/bt_bridge_thread.d \
./src/synergy_gen/bt_thread.d \
./src/synergy_gen/cli_thread.d \
./src/synergy_gen/common_data.d \
./src/synergy_gen/hal_data.d \
./src/synergy_gen/main.d \
./src/synergy_gen/output_thread.d \
./src/synergy_gen/pin_data.d \
./src/synergy_gen/wifi_thread.d 


# Each subdirectory must supply rules for building sources it contributes
src/synergy_gen/%.o: ../src/synergy_gen/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -I"C:\git_hbird\meg_board\synergy_cfg\ssp_cfg\bsp" -I"C:\git_hbird\meg_board\synergy_cfg\ssp_cfg\driver" -I"C:\git_hbird\meg_board\synergy\ssp\inc" -I"C:\git_hbird\meg_board\synergy\ssp\inc\bsp" -I"C:\git_hbird\meg_board\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git_hbird\meg_board\synergy\ssp\inc\driver\api" -I"C:\git_hbird\meg_board\synergy\ssp\inc\driver\instances" -I"C:\git_hbird\meg_board\src" -I"C:\git_hbird\meg_board\src\synergy_gen" -I"C:\git_hbird\meg_board\synergy_cfg\ssp_cfg\framework\el" -I"C:\git_hbird\meg_board\synergy\ssp\inc\framework\el" -I"C:\git_hbird\meg_board\synergy\ssp\src\framework\el\tx" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


