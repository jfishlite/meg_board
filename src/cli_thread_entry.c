//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: cli_thread_entry.c is responsible for processing CLI commands
//  (sent by someone via the CLI serial interface) and transmitting back the
//  results of the command.
//
//-----------------------------------------------------------------------------

//    Include Files
//***************************************************************************
#include <time.h>
#include "cli_thread.h"
#include "cli_uart_driver.h"


//    Constants & Definitions  (File-scope)
//****************************************************************************
#define MAX_NAME     20
#define MAX_ARGS     10


//    Structures, enums, etc.  (File-scope)
//****************************************************************************
// callback function type and prototype
typedef void (*handler_ptr_t)(uint16_t, char**, int16_t);
typedef struct
{
  char          command[MAX_NAME];
  char         *params[MAX_ARGS];
  uint16_t      params_num;
  handler_ptr_t cmd_handler;
 } cmdLineEntry_t;


//    Function prototypes - private members
//****************************************************************************
static char *cli_reset_cmdLine(char *buf);
static void cli_logo_display(void);
static void cli_help(uint16_t argc, char *argv[], int16_t cmdNDX);
static void cli_show(uint16_t argc, char *argv[], int16_t cmdNDX);
static void cli_get_timedate(uint16_t argc, char *argv[], int16_t cmdNDX);
static void cli_set_timedate(uint16_t argc, char *argv[], int16_t cmdNDX);
static int16_t cli_parse_cmdLine(char *buf, cmdLineEntry_t *cmdargs);
static void cli_invalid_arg_count(int16_t cmdNDX);
static void cli_help_detail(int16_t cmnNDX);


//    Variable declarations  (File-scope / static)
//****************************************************************************
// table of <commands>
static const char *commands[] = { "help",
                                  "show",
                                  "gettd",
                                  "settd" };

// **Note: Command list(above) and function table (below) arrays must be sync'ed!

// define table functions pointers
static handler_ptr_t ftable[] = { cli_help,
                                  cli_show,
                                  cli_get_timedate,
                                  cli_set_timedate };

const char           *PROMPT = "MEG> ";
static char           cli_buffer[CLI_BUFSIZE] = {0};
static cmdLineEntry_t cli = {{0}, {NULL}, 0, NULL};


//****************************************************************************
//    Code Starts Here
//****************************************************************************

//--------------------------------------------------------------------------------
// NAME      : cli_thread_entry
// ABSTRACT  : This function gets the interrupt status of one of the pushbuttons.
// Latches occurrence of an interrupt. Once the value for a button has been read
// the value is then cleared.
// ARGUMENTS :
// PUSHBUTTON_E pushbutton_index - index to the desired pushbutton
// RETURN    :
// status of the pushbutton
//--------------------------------------------------------------------------------
void cli_thread_entry(void)
{
    char *ptr = cli_buffer;
    uint8_t    byte;
    uint8_t    len = 0;
    int16_t   cmdNDX;

    // Transmit out the startup Logo (and FW version)
    cli_logo_display();

    while (1)
    {
        if(cli_uart_dataAvailable() == true)
        {
            // Retrieve a character from the Console
            byte = cli_uart_getByte();

            if(byte)
            {
                DebugOutput("%c",byte);                //echo
                if(byte == '\n' || byte == '\r')
                {
                    // skip over LF/SP/CR/TAB/NULL
                    if( (cli_buffer[0] == '\n') || (cli_buffer[0] == ' ')  ||
                        (cli_buffer[0] == '\r') || (cli_buffer[0] == '\t') ||
                        (cli_buffer[0] == '\0') )
                    {
                        ptr = cli_reset_cmdLine(cli_buffer);
                        len = 0;
                        continue;
                    }

                    // parse stream
                    cmdNDX = cli_parse_cmdLine(cli_buffer, &cli);
                    if(cmdNDX < 0)
                    {
                        DebugOutput("\r\nError 1: invalid command!");
                        cli_help(cli.params_num, cli.params, cmdNDX);
                        ptr = cli_reset_cmdLine(cli_buffer);
                        len = 0;
                        continue;
                    }
                    else
                    {
                        cli.cmd_handler(cli.params_num, cli.params, cmdNDX);
                        ptr = cli_reset_cmdLine(cli_buffer);
                        len = 0;
                        continue;
                    }
                }

                // Advance to the next byte in the cmd
                *ptr ++= (char)byte;
                if(++len > CLI_BUFSIZE)
                {
                    DebugOutput("\r\nError 2: invalid command!\r\n");
                    ptr = cli_reset_cmdLine(cli_buffer);
                    len = 0;
                }
            }
        }
        else
        {
            tx_thread_sleep(1);
        }
    }
}


//    Private Members (static)
//******************************************************************************

//--------------------------------------------------------------------------------
// NAME      : cli_reset_cmdLine
// ABSTRACT  : This function erases the rx buffer and displays the prompt.
// ARGUMENTS :
//   buf - pointer to the rx buffer
// RETURN    : None
//--------------------------------------------------------------------------------
static char *cli_reset_cmdLine(char *buf)
{
  memset(buf, 0, CLI_BUFSIZE);
  DebugOutput("\r\n%s",PROMPT);
  return buf;
}


//--------------------------------------------------------------------------------
// NAME      : cli_logo_display
// ABSTRACT  : This function displays the startup logo and cmd prompt.
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
static void cli_logo_display(void)
{
    DebugOutput("\r\n\r\n        Software version: ");
//    DebugOutput(SOFTWARE_VERSION);
    DebugOutput("\r\n        Intermatic Inc. MEG board");
    DebugOutput("\r\n            7777 Winn Rd.");
    DebugOutput("\r\n          Spring Grove, IL 60081");
    DebugOutput("\r\n          Phone: 815-675-7000\r\n");
    DebugOutput("\r\n%s", PROMPT);
}


//--------------------------------------------------------------------------------
// NAME      : cli_help
// ABSTRACT  : This function displays the menu of valid commands.
// ARGUMENTS :
//   argc - Number of arguments in cmd (excluding the cmd itself)
//   argv - null-delimited arguments
//   cmdNDX - The original cmd
// RETURN    : None
//--------------------------------------------------------------------------------
static void cli_help(uint16_t argc, char *argv[], int16_t cmdNDX)
{
    //rdmTODO
    SSP_PARAMETER_NOT_USED(argc);
    SSP_PARAMETER_NOT_USED(argv);
    SSP_PARAMETER_NOT_USED(cmdNDX);
}


//--------------------------------------------------------------------------------
// NAME      : cli_show
// ABSTRACT  : This function processes the "show" cmd.
// ARGUMENTS :
//   argc - Number of arguments in cmd (excluding the cmd itself)
//   argv - null-delimited arguments
//   cmdNDX - The original cmd
// RETURN    : None
//--------------------------------------------------------------------------------
static void cli_show(uint16_t argc, char *argv[], int16_t cmdNDX)
{
    SSP_PARAMETER_NOT_USED(argc);
    SSP_PARAMETER_NOT_USED(argv);
    SSP_PARAMETER_NOT_USED(cmdNDX);
    cli_logo_display();
}


//--------------------------------------------------------------------------------
// NAME      : cli_get_timedate
// ABSTRACT  : This function processes the "gettd" cmd.
// ARGUMENTS :
//   argc - Number of arguments in cmd (excluding the cmd itself)
//   argv - null-delimited arguments
//   cmdNDX - The original cmd
// RETURN    : None
//--------------------------------------------------------------------------------
static void cli_get_timedate(uint16_t argc, char *argv[], int16_t cmdNDX)
{
    //rdmTODO
    SSP_PARAMETER_NOT_USED(argc);
    SSP_PARAMETER_NOT_USED(argv);
    SSP_PARAMETER_NOT_USED(cmdNDX);
}


//--------------------------------------------------------------------------------
// NAME      : cli_set_timedate
// ABSTRACT  : This function processes the "settd" cmd.
// ARGUMENTS :
//   argc - Number of arguments in cmd (excluding the cmd itself)
//   argv - null-delimited arguments
//   cmdNDX - The original cmd
// RETURN    : None
//--------------------------------------------------------------------------------
static void cli_set_timedate(uint16_t argc, char *argv[], int16_t cmdNDX)
{
    struct tm timedate;

    if(argc == 5)
    {
        memset(&timedate, 0, sizeof(struct tm));
        timedate.tm_mon = atoi(argv[0]) - 1;   // Subtract 1 because time is stored as Jan=0
        timedate.tm_mday = atoi(argv[1]);
        timedate.tm_year = atoi(argv[2]);  // Stored as # years since 1900
        timedate.tm_hour = atoi(argv[3]);
        timedate.tm_min = atoi(argv[4]);

        if (true)
        {
            DebugOutput("\r\nTime & Date have been saved.\r\n");
        }
        else
        {
            DebugOutput("\r\nInvalid TimeDate.\r\n");
        }
    }
    else
    {
        cli_invalid_arg_count(cmdNDX);
    }
}


//--------------------------------------------------------------------------------
// NAME      : cli_parse_cmdLine
// ABSTRACT  : This function parses the cmd.
// ARGUMENTS :
//   buf - Number of arguments in cmd (excluding the cmd itself)
// RETURN    : None
//--------------------------------------------------------------------------------
static int16_t cli_parse_cmdLine(char *buf, cmdLineEntry_t *cmdargs)
{
    const char delim[] = " \t\n";
    uint16_t   cmdNDX=0;
    uint16_t   argCNT=0;
    char       *token;

    memset(cmdargs, 0, sizeof(cmdLineEntry_t));
    token = strtok(buf, delim);

    for (cmdNDX = 0; cmdNDX < COUNT(commands); cmdNDX++)
    {
        if(!strcmp(token, commands[cmdNDX]))
        {
            strcpy(cmdargs->command, token);
            cmdargs->cmd_handler = ftable[cmdNDX];
            for(token = strtok(NULL, delim); token != NULL;)
            {
                cmdargs->params[argCNT++] =  token;
                token = strtok(NULL, delim);
            }
            cmdargs->params_num = argCNT;
            return (int16_t)cmdNDX; // return the command index in ftable[]
        }
    }
    return -1;
}


//--------------------------------------------------------------------------------
// NAME      : cli_invalid_arg_count
// ABSTRACT  : This function parses the cmd.
// ARGUMENTS :
//   cmdNDX - The cmd issued by the user of the CLI interface.
// RETURN    : None
//--------------------------------------------------------------------------------
static void cli_invalid_arg_count(int16_t cmdNDX)
{
    DebugOutput("\r\nInvalid number of arguments.\r\n");
    cli_help_detail(cmdNDX);
}


//--------------------------------------------------------------------------------
// NAME      : cli_help_detail
// ABSTRACT  : This function pprovides details about each cmd.
// ARGUMENTS :
//   cmdNDX - The cmd issued by the user of the CLI interface.
// RETURN    : None
//--------------------------------------------------------------------------------
static void cli_help_detail(int16_t cmnNDX)
{
    switch(cmnNDX)
    {
        case 0:  // help
            DebugOutput("\r\nhelp         - Display this message");
            break;

        case 1:  // show
            DebugOutput("\r\nshow         - Display Software version and " \
                                          "Intermatic contact information");
            break;

        case 2:  // gettd
            DebugOutput("\r\ngettd        - Display current Time & Date in 24-hour format along with" \
                        "\r\n               DST active or inactive (e.g. 11/09/16 13:50 DST-0)");
            break;

        case 3:  // settd
            DebugOutput("\r\nsettd        - Set Time & Date with 5 arguments (MM DD YY HH mm)" \
                        "\r\n               (e.g. 11 9 16 13 50 sets timer to: 11/9/2016 13:50)");
            break;

        default:
            DebugOutput("\r\nError 4: invalid command!");
            break;
    }
}
