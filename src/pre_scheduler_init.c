//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: pre_scheduler_init contains functions called before OS
// scheduler is started.
//
//-----------------------------------------------------------------------------
#include <bt_dtm_uart_driver.h>
#include "pre_scheduler_init.h"
#include "adc_driver.h"
#include "pwm_driver.h"
#include "bt_dtm_uart_driver.h"
#include "bt_bridge_uart_driver.h"
#include "wifi_uart_driver.h"
#include "cli_uart_driver.h"
#include "mac_i2c_driver.h"
#include "rtc_i2c_driver.h"
#include "hw_io_api.h"
#include "pushbutton_driver.h"

//--------------------------------------------------------------------------------
// NAME      : tx_application_define_user
// ABSTRACT  : Function to initialize drivers, functions, etc after threads
// are created and before scheduler starts. Called by tx_application_define.
//
// ARGUMENTS : None
// RETURN    :
//   None
//--------------------------------------------------------------------------------
void tx_application_define_user(void *first_unused_memory)
{
    (void) ((first_unused_memory)); // unused parameter

    init_pushbutton(); // initialize pushbutton driver

    init_adc_driver(); // initialize adc drivers

    init_pwm_driver(); // initialize pwm drivers

    init_bt_dtm_uart(); // initialize bt dtm comm port

    init_bt_bridge_uart(); // initialize bt bridge comm port

    init_wifi_uart(); // initialize wifi comm port

    init_cli_uart(); // initialize cli comm port

    init_mac_i2c(); // initialize mac i2c comm

    init_rtc_i2c(); // initialize rtc i2c comm

    //init_watchdog_driver(); //initialize watchdog timer

    hw_io_create_tx_timer(); // create tx timer
}
