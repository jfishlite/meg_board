//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: adc_driver.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __PUSHBUTTON_DRIVER_H__
#define __PUSHBUTTON_DRIVER_H__
#include "hal_data.h"

typedef enum {
    BUTTON_PRESSED  = 1,
    BUTTON_RELEASED = 0
} BUTTON_E;

// KEY MATRIX channels
typedef enum {
MODE_SW      = 0x01,
NETWORK_SW   = 0x02,
RLY1_OVRD_SW = 0x04,
RLY2_OVRD_SW = 0x08,
RLY3_OVRD_SW = 0x10,
RLY4_OVRD_SW = 0x20,
RESET_SW     = 0x40,
BLUETOOTH_SW = 0x80
} PUSHBUTTON_E;

typedef struct{
    BUTTON_E mode_sw;
    BUTTON_E network_sw;
    BUTTON_E rly1_ovrd_sw;
    BUTTON_E rly2_ovrd_sw;
    BUTTON_E rly3_ovrd_sw;
    BUTTON_E rly4_ovrd_sw;
    BUTTON_E reset_sw;
    BUTTON_E bluetooth_sw;
}button_channels_t;

ssp_err_t get_pushbutton_status(PUSHBUTTON_E pushbutton_index, ioport_level_t button_status);
BUTTON_E get_int_pushbutton_status(PUSHBUTTON_E pushbutton_index);
void init_pushbutton(void);
void pushbutton_interrupt(keymatrix_callback_args_t *p_args);

#endif // __PUSHBUTTON_DRIVER_H__
