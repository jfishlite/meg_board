//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: pushbutton_driver.c
//
//-----------------------------------------------------------------------------
#include "pushbutton_driver.h"
#include "hal_data.h"
#include "hw_io_api.h"

static button_channels_t button_struct = {BUTTON_RELEASED, BUTTON_RELEASED, BUTTON_RELEASED, BUTTON_RELEASED,
                                        BUTTON_RELEASED, BUTTON_RELEASED, BUTTON_RELEASED, BUTTON_RELEASED};

//--------------------------------------------------------------------------------
// NAME      : get_pushbutton_status
// ABSTRACT  : This function gets the current level of one of the pushbuttons
// ARGUMENTS :
// PUSHBUTTON_E pushbutton_index    - index to the desired pushbutton
// ioport_level_t button_status     - pushbutton status (input level)
// RETURN    :
// status of the read operation
//--------------------------------------------------------------------------------
ssp_err_t get_pushbutton_status(PUSHBUTTON_E pushbutton_index, ioport_level_t button_status)
{
    ssp_err_t status = SUCCESS;

    switch(pushbutton_index)
    {
    case MODE_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_00, &button_status);
        break;
    case NETWORK_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_01, &button_status);
        break;
    case RLY1_OVRD_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_02, &button_status);
        break;
    case RLY2_OVRD_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_03, &button_status);
        break;
    case RLY3_OVRD_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_04, &button_status);
        break;
    case RLY4_OVRD_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_05, &button_status);
        break;
    case RESET_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_06, &button_status);
        break;
    case BLUETOOTH_SW:
        status = g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_07, &button_status);
        break;
    }
    return (status);
}

//--------------------------------------------------------------------------------
// NAME      : get_int_pushbutton_status
// ABSTRACT  : This function gets the interrupt status of one of the pushbuttons.
// Latches occurrence of an interrupt. Once the value for a button has been read
// the value is then cleared.
// ARGUMENTS :
// PUSHBUTTON_E pushbutton_index - index to the desired pushbutton
// RETURN    :
// status of the pushbutton
//--------------------------------------------------------------------------------
BUTTON_E get_int_pushbutton_status(PUSHBUTTON_E pushbutton_index)
{
    BUTTON_E button_status = BUTTON_RELEASED;

    switch(pushbutton_index)
    {
    case MODE_SW:
        button_status = button_struct.mode_sw;
        button_struct.mode_sw = BUTTON_RELEASED;
        break;
    case NETWORK_SW:
        button_status = button_struct.network_sw;
        button_struct.network_sw = BUTTON_RELEASED;
        break;
    case RLY1_OVRD_SW:
        button_status = button_struct.rly1_ovrd_sw;
        button_struct.rly1_ovrd_sw = BUTTON_RELEASED;
        break;
    case RLY2_OVRD_SW:
        button_status = button_struct.rly2_ovrd_sw;
        button_struct.rly2_ovrd_sw = BUTTON_RELEASED;
        break;
    case RLY3_OVRD_SW:
        button_status = button_struct.rly3_ovrd_sw;
        button_struct.rly3_ovrd_sw = BUTTON_RELEASED;
        break;
    case RLY4_OVRD_SW:
        button_status = button_struct.rly4_ovrd_sw;
        button_struct.rly4_ovrd_sw = BUTTON_RELEASED;
        break;
    case RESET_SW:
        button_status = button_struct.reset_sw;
        button_struct.reset_sw = BUTTON_RELEASED;
        break;
    case BLUETOOTH_SW:
        button_status = button_struct.bluetooth_sw;
        button_struct.bluetooth_sw = BUTTON_RELEASED;
        break;
    }
    return (button_status);
}

//--------------------------------------------------------------------------------
// NAME      : pushbutton_init
// ABSTRACT  : This function initializes the pushbutton (keymatrix) driver
// ARGUMENTS :
// RETURN    :
//--------------------------------------------------------------------------------
void init_pushbutton(void)
{
    g_kint0.p_api->open(g_kint0.p_ctrl, g_kint0.p_cfg);

    button_struct.mode_sw = BUTTON_RELEASED;
    button_struct.network_sw = BUTTON_RELEASED;
    button_struct.rly1_ovrd_sw = BUTTON_RELEASED;
    button_struct.rly2_ovrd_sw = BUTTON_RELEASED;
    button_struct.rly3_ovrd_sw = BUTTON_RELEASED;
    button_struct.rly4_ovrd_sw = BUTTON_RELEASED;
    button_struct.reset_sw = BUTTON_RELEASED;
    button_struct.bluetooth_sw = BUTTON_RELEASED;
}

//--------------------------------------------------------------------------------
// NAME      : pushbutton_interrupt
// ABSTRACT  : This function is the interrupt handler for the keymatrix
// ARGUMENTS :
// RETURN    :
//--------------------------------------------------------------------------------
void pushbutton_interrupt(keymatrix_callback_args_t *p_args)
{

    switch(p_args->channels)
    {
    case MODE_SW:
        button_struct.mode_sw = BUTTON_PRESSED;
        break;
    case NETWORK_SW:
        button_struct.network_sw = BUTTON_PRESSED;
        break;
    case RLY1_OVRD_SW:
        button_struct.rly1_ovrd_sw = BUTTON_PRESSED;
        break;
    case RLY2_OVRD_SW:
        button_struct.rly2_ovrd_sw = BUTTON_PRESSED;
        break;
    case RLY3_OVRD_SW:
        button_struct.rly3_ovrd_sw = BUTTON_PRESSED;
        break;
    case RLY4_OVRD_SW:
        button_struct.rly4_ovrd_sw = BUTTON_PRESSED;
        break;
    case RESET_SW:
        button_struct.reset_sw = BUTTON_PRESSED;
        break;
    case BLUETOOTH_SW:
        button_struct.bluetooth_sw = BUTTON_PRESSED;
        break;
    }
}
