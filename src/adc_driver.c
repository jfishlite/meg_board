//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: adc_driver.c
//
//-----------------------------------------------------------------------------
#include "adc_driver.h"
#include "input_thread.h"
//--------------------------------------------------------------------------------
// NAME      : init_adc_driver
// ABSTRACT  : This function initializes the adc driver
// ARGUMENTS :
// RETURN    :
//--------------------------------------------------------------------------------
void init_adc_driver(void)
{
    // init adc0
    g_adc0.p_api->open(g_adc0.p_ctrl, g_adc0.p_cfg);
    g_adc0.p_api->scanCfg(g_adc0.p_ctrl,g_adc0.p_channel_cfg);

    // init adc1
    g_adc1.p_api->open(g_adc1.p_ctrl, g_adc1.p_cfg);
    g_adc1.p_api->scanCfg(g_adc1.p_ctrl,g_adc1.p_channel_cfg);
}
