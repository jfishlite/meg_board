#include "mac_i2c_driver.h"
#include "hal_data.h"

void init_mac_i2c(void)
{
    mac_i2c0.p_api->open(mac_i2c0.p_ctrl, mac_i2c0.p_cfg);
}

void mac_callback(i2c_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
