//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: hw_io_api.c is responsible for reading analog inputs,
//  setting enable outputs for zc, setting PWM output values.
//
//-----------------------------------------------------------------------------
#include "hw_io_api.h"
#include "common_def.h"
#include "pwm_driver.h"
#include "input_thread.h"
#include "output_thread.h"
#include "hw_config.h"
#include "pushbutton_driver.h"

typedef enum {
    HW_IO_EXPIRED     = 1,
    HW_IO_NOT_EXPIRED = 0
} HW_IO_TIMER_E;

#define TIMER_VALUE            1 // value passed as arg to callback
#define HW_IO_TIMEOUT          2 // 2 * 10 msec timeout timer ticks = 20 msec timeout value
#define ONE_SHOT               0 // one shot timer

TX_TIMER hw_io_timer;

static HW_IO_TIMER_E hw_io_timeout = HW_IO_NOT_EXPIRED;

//--------------------------------------------------------------------------------
// NAME      : hw_io_timeout_callback
// ABSTRACT  : Callback function for software timer hw_io_timer.
//             If the timer expires this callback function is called.
// ARGUMENTS :
//  unsigned long int expire_input - input when timer expires
// RETURN    :
//--------------------------------------------------------------------------------
static void hw_io_timeout_callback(unsigned long int expire_input)
{
    // This is to avoid compiler complaining about the parameter
    // not used.
    SSP_PARAMETER_NOT_USED(expire_input);

    // timer expired set flag
    hw_io_timeout = HW_IO_EXPIRED;

}

//--------------------------------------------------------------------------------
// NAME      : hw_io_create_tx_timer
// ABSTRACT  : This function creates a software timer
//             If the timer expires the callback function hw_io_timeout_callback
//             will be called.
// ARGUMENTS :
// RETURN    :
//   STATUS_E  result of the function execution
//--------------------------------------------------------------------------------
STATUS_E hw_io_create_tx_timer(void)
{

    if (tx_timer_create(&hw_io_timer,
                        "hw_io_Timeout",
                        hw_io_timeout_callback,
                        TIMER_VALUE,
                        HW_IO_TIMEOUT,
                        ONE_SHOT,
                        TX_NO_ACTIVATE) != SUCCESS)
    {
        return FAILURE;
    }

    return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : hw_read_adc_channels
// ABSTRACT  : This API reads ADC channels returns the raw value of each channel
//             in the structure passed into the function
// ARGUMENTS :
//   adc_channels_t *adc_data_ptr   - structure that contains the analog channels
// RETURN    :
//   STATUS_E  result of the function execution
//--------------------------------------------------------------------------------
STATUS_E hw_read_adc_channels(adc_channels_t *adc_data_ptr)
{
    STATUS_E  status = SUCCESS;
    //
    // get channels for adc 0
    //
    // scan a/d 0
    g_adc0.p_api->scanStart(g_adc0.p_ctrl);
    hw_io_timeout = HW_IO_NOT_EXPIRED;
    tx_timer_change(&hw_io_timer, HW_IO_TIMEOUT, ONE_SHOT);
    tx_timer_activate(&hw_io_timer);
    //
    while ((g_adc0.p_api->scanStatusGet(g_adc0.p_ctrl) != SSP_SUCCESS)&&
            (hw_io_timeout != HW_IO_EXPIRED))
    {
    }

    g_adc0.p_api->scanStop(g_adc0.p_ctrl);

    if (hw_io_timeout == HW_IO_EXPIRED)
    {
        status = FAILURE;
        return status;
    }
                                                                                                // pin#    peripheral     schematic
    g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_0, (uint16_t*)&adc_data_ptr->pwr_n);      // P000     ADC0:AN00       PWR_IN
    g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_1, (uint16_t*)&adc_data_ptr->analog1);    // P001     ADC0:AN01       ANALOG1
    g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_2, (uint16_t*)&adc_data_ptr->analog2);    // P002     ADC0:AN02       ANALOG2
    g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_3, (uint16_t*)&adc_data_ptr->analog3);    // P008     ADC0:AN03       ANALOG3
    g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_4, (uint16_t*)&adc_data_ptr->analog4);    // P009     ADC0:AN04       ANALOG4
    //
    // get channels for adc 1
    //
    // scan a/d 1
    g_adc1.p_api->scanStart(g_adc1.p_ctrl);
    hw_io_timeout = HW_IO_NOT_EXPIRED;
    tx_timer_change(&hw_io_timer, HW_IO_TIMEOUT, ONE_SHOT);
    tx_timer_activate(&hw_io_timer);
    // see if adc1 scan is complete
    while ((g_adc1.p_api->scanStatusGet(g_adc1.p_ctrl) != SSP_SUCCESS)&&
            (hw_io_timeout != HW_IO_EXPIRED))
    {
    }

    g_adc1.p_api->scanStop(g_adc1.p_ctrl);

    if (hw_io_timeout == HW_IO_EXPIRED)
    {
        status = FAILURE;
        return status;
    }
                                                                                                // pin#    peripheral     schematic
    g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_0, (uint16_t*)&adc_data_ptr->current1);   // P004    ADC1:AN00      CURRENT1
    g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_1, (uint16_t*)&adc_data_ptr->current2);   // P005    ADC1:AN01      CURRENT2
    g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_2, (uint16_t*)&adc_data_ptr->current3);   // P006    ADC1:AN02      CURRENT3
    g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_7, (uint16_t*)&adc_data_ptr->current4);   // P007    ADC1:AN07      CURRENT4

    return status;
}
//--------------------------------------------------------------------------------
// NAME      : hw_read_digital_input
// ABSTRACT  : This function will read the digital inputs DIGIN1, 2, 3, 4 one at
// time.
// ARGUMENTS :
//   DIGIN_ID_E digital_input     - the selected input to read
//   ioport_level_t * port_status - the status of the selected port
// RETURN    :
//   STATUS_E       result of the function execution
//--------------------------------------------------------------------------------
void hw_read_digital_input(DIGIN_ID_E digital_input, ioport_level_t * port_status)
{

    switch(digital_input)
    {
    case DIGINPUT_1:
        g_ioport.p_api->pinRead(DIGIN1, port_status);
        break;
    case DIGINPUT_2:
        g_ioport.p_api->pinRead(DIGIN2, port_status);
        break;
    case DIGINPUT_3:
        g_ioport.p_api->pinRead(DIGIN3, port_status);
        break;
    case DIGINPUT_4:
        g_ioport.p_api->pinRead(DIGIN4, port_status);
        break;
    }
}
//--------------------------------------------------------------------------------
// NAME      : hw_set_led
// ABSTRACT  : This function will write data to one of the LED latches
//
// ARGUMENTS :
//  LATCH_ID_E latch_id                 - the selected latch
//  uint8_t  address                    - address of led element
//  LED_STATE_ID_E led_data             - led_data on / off
// RETURN    :
//   STATUS_E       result of the function execution
//--------------------------------------------------------------------------------
void hw_set_led(LATCH_ID_E latch_id, uint8_t  address, LED_STATE_ID_E led_data)
{

    // enable the latch
    switch(latch_id)
    {
    case LATCH1:
        g_ioport.p_api->pinWrite(LATCH_MR1, IOPORT_LEVEL_HIGH);
        break;
    case LATCH2:
        g_ioport.p_api->pinWrite(LATCH_MR2, IOPORT_LEVEL_HIGH);
        break;
    case LATCH3:
        g_ioport.p_api->pinWrite(LATCH_MR3, IOPORT_LEVEL_HIGH);
        break;
    }

    switch(address)
    {
    case 000:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_LOW);
         break;
    case 001:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_LOW);
         break;
    case 002:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_LOW);
        break;
    case 003:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_LOW);
        break;
    case 004:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_HIGH);
        break;
    case 005:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_HIGH);
        break;
    case 006:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_LOW);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_HIGH);
        break;
    case 007:
         g_ioport.p_api->pinWrite(LATCH_A0, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A1, IOPORT_LEVEL_HIGH);
         g_ioport.p_api->pinWrite(LATCH_A2, IOPORT_LEVEL_HIGH);
        break;
    }
    //
    // write data
    //
    g_ioport.p_api->pinWrite(LED_DATA, led_data);
    //
    // enable the latch (LOW)
    //
    switch(latch_id)
    {
    case LATCH1:
        g_ioport.p_api->pinWrite(LATCH_EN1, IOPORT_LEVEL_LOW);
        break;
    case LATCH2:
        g_ioport.p_api->pinWrite(LATCH_EN2, IOPORT_LEVEL_LOW);
        break;
    case LATCH3:
        g_ioport.p_api->pinWrite(LATCH_EN3, IOPORT_LEVEL_LOW);
        break;
    }
    //
    // DELAY FOR 1 MILLISECOND
    R_BSP_SoftwareDelay(1, BSP_DELAY_UNITS_MILLISECONDS);
    //
    // disable the latch (HIGH)
    //
    switch(latch_id)
    {
    case LATCH1:
        g_ioport.p_api->pinWrite(LATCH_EN1, IOPORT_LEVEL_HIGH);
        break;
    case LATCH2:
        g_ioport.p_api->pinWrite(LATCH_EN2, IOPORT_LEVEL_HIGH);
        break;
    case LATCH3:
        g_ioport.p_api->pinWrite(LATCH_EN3, IOPORT_LEVEL_HIGH);
        break;
    }

}

//--------------------------------------------------------------------------------
// NAME      : hw_set_zc_enable
// ABSTRACT  : This API enables/disables the Zero-Cross function of a relay.
// ARGUMENTS :
//  ZC_RELAY_ID_E zc_relay_id   -  id of the relay of the ZC function;
//  ZC_ENABLE_E zc_enable       - set the ZC function to DISABLE/NOT_DISABLE
// RETURN    :
//   STATUS_E       result of the function execution
//--------------------------------------------------------------------------------
void hw_set_zc_enable(ZC_RELAY_ID_E zc_relay_id, ZC_ENABLE_E zc_enable)
{

    switch (zc_relay_id)
    {
      case ZC_RELAY_1:
          if (zc_enable == ZC_ENABLE)
              g_ioport.p_api->pinWrite(R1_ZC_EN, IOPORT_LEVEL_HIGH);
          else
              g_ioport.p_api->pinWrite(R1_ZC_EN, IOPORT_LEVEL_LOW);
        break;

      case ZC_RELAY_2:
          if (zc_enable == ZC_ENABLE)
              g_ioport.p_api->pinWrite(R2_ZC_EN, IOPORT_LEVEL_HIGH);
          else
              g_ioport.p_api->pinWrite(R2_ZC_EN, IOPORT_LEVEL_LOW);
        break;

      case ZC_RELAY_3:
          if (zc_enable == ZC_ENABLE)
              g_ioport.p_api->pinWrite(R3_ZC_EN, IOPORT_LEVEL_HIGH);
          else
              g_ioport.p_api->pinWrite(R3_ZC_EN, IOPORT_LEVEL_LOW);
        break;

    }

}

//--------------------------------------------------------------------------------
// NAME      : hw_test
// ABSTRACT  : Calls API functions for testing hw io functions. Called before a
// thread starts. For test only.
// ARGUMENTS :
// RETURN    :
//--------------------------------------------------------------------------------
void hw_test(void)
{
    adc_channels_t adc_data;
    ioport_level_t port_status;

    //
    // test LEDs
    //
    // turn ON LEDs
    for(uint8_t i = 0; i < 8; i++)
    {
        hw_set_led(LATCH1, i, LED_ON);
        R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
    }

    for(uint8_t i = 0; i < 8; i++)
    {
        hw_set_led(LATCH2, i, LED_ON);
        R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
    }

    for(uint8_t i = 0; i < 8; i++)
    {
        hw_set_led(LATCH3, i, LED_ON);
        R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
    }


    // turn OFF LEDs
    for(uint8_t i = 0; i < 8; i++)
    {
        hw_set_led(LATCH1, i, LED_OFF);
        R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
    }

    for(uint8_t i = 0; i < 8; i++)
    {
        hw_set_led(LATCH2, i, LED_OFF);
        R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
    }

    for(uint8_t i = 0; i < 8; i++)
    {
        hw_set_led(LATCH3, i, LED_OFF);
        R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
    }




    //
    // test switches
    //
    // turn on leds
    while(get_int_pushbutton_status(MODE_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 0, LED_OFF);
    }
    hw_set_led(LATCH3, 0, LED_ON);
    //
    while(get_int_pushbutton_status(NETWORK_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 1, LED_OFF);
    }
    hw_set_led(LATCH3, 1, LED_ON);
    //
    while(get_int_pushbutton_status(RLY1_OVRD_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 2, LED_OFF);
    }
    hw_set_led(LATCH3, 2, LED_ON);
    //
    while(get_int_pushbutton_status(RLY2_OVRD_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 3, LED_OFF);
    }
    hw_set_led(LATCH3, 3, LED_ON);
    //
    while(get_int_pushbutton_status(RLY3_OVRD_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 4, LED_OFF);
    }
    hw_set_led(LATCH3, 4, LED_ON);
    //
    while(get_int_pushbutton_status(RLY4_OVRD_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 5, LED_OFF);
    }
    hw_set_led(LATCH3, 5, LED_ON);
    //
    while(get_int_pushbutton_status(RESET_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 6, LED_OFF);
    }
    hw_set_led(LATCH3, 6, LED_ON);
    //
    while(get_int_pushbutton_status(BLUETOOTH_SW) == BUTTON_RELEASED)
    {
        hw_set_led(LATCH3, 7, LED_OFF);
    }
    hw_set_led(LATCH3, 7, LED_ON);

    // test switches
    //
    // turn off leds
    while(get_int_pushbutton_status(MODE_SW) == BUTTON_RELEASED)
    {

    }
    hw_set_led(LATCH3, 0, LED_OFF);
    //
    while(get_int_pushbutton_status(NETWORK_SW) == BUTTON_RELEASED)
    {
    }
    hw_set_led(LATCH3, 1, LED_OFF);
    //
    while(get_int_pushbutton_status(RLY1_OVRD_SW) == BUTTON_RELEASED)
    {
    }
    hw_set_led(LATCH3, 2, LED_OFF);
    //
    while(get_int_pushbutton_status(RLY2_OVRD_SW) == BUTTON_RELEASED)
    {
    }
    hw_set_led(LATCH3, 3, LED_OFF);
    //
    while(get_int_pushbutton_status(RLY3_OVRD_SW) == BUTTON_RELEASED)
    {
    }
    hw_set_led(LATCH3, 4, LED_OFF);
    //
    while(get_int_pushbutton_status(RLY4_OVRD_SW) == BUTTON_RELEASED)
    {
    }
    hw_set_led(LATCH3, 5, LED_OFF);
    //
    while(get_int_pushbutton_status(RESET_SW) == BUTTON_RELEASED)
    {
    }
    hw_set_led(LATCH3, 6, LED_OFF);
    //
    while(get_int_pushbutton_status(BLUETOOTH_SW) == BUTTON_RELEASED)
    {
    }
    hw_set_led(LATCH3, 7, LED_OFF);



    // read digital inputs
    hw_read_digital_input(DIGINPUT_1, &port_status);
    hw_read_digital_input(DIGINPUT_2, &port_status);
    hw_read_digital_input(DIGINPUT_3, &port_status);
    hw_read_digital_input(DIGINPUT_4, &port_status);

    // read adc channels for both adc's
    hw_read_adc_channels(&adc_data);

    // for test use these initial values
    hw_set_pwm_output(PWM1, 10);
    hw_set_pwm_output(PWM2, 30);
    hw_set_pwm_output(PWM3, 50);
    hw_set_pwm_output(PWM4, 70);

    // close relays
    hw_set_relay(RELAY_1, CLOSE);
    hw_set_relay(RELAY_2, CLOSE);
    hw_set_relay(RELAY_3, CLOSE);
    hw_set_relay(PHASE_RELAY, CLOSE);

    // open relays
    hw_set_relay(RELAY_1, OPEN);
    hw_set_relay(RELAY_2, OPEN);
    hw_set_relay(RELAY_3, OPEN);
    hw_set_relay(PHASE_RELAY, OPEN);
    // for test
}

//--------------------------------------------------------------------------------
// NAME      : hw_set_pwm_output
// ABSTRACT  : This API sets PWM (Pulse Width Modulation)
//             output, i.e., percent duty cycle 0-100%.
// ARGUMENTS :
//   PWM_ID_E pwm_id    -  id of the channel;
//   unsigned pwm_value - pwm value 0 - 100 %
// RETURN    :
//   STATUS_E  result of the function execution
//
//--------------------------------------------------------------------------------
STATUS_E hw_set_pwm_output(PWM_ID_E pwm_id, unsigned pwm_value)
{
    STATUS_E  status = FAILURE;
    ssp_err_t ssp_err = SSP_SUCCESS;

    // invert PWM for new hardware
    pwm_value = 100 - pwm_value;

    switch(pwm_id)
    {
        case PWM1:
            ssp_err = g_timer2_pwm_1.p_api->dutyCycleSet(g_timer2_pwm_1.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,A_OUTPUT);
            break;
        case PWM2:
            ssp_err = g_timer0_pwm_2.p_api->dutyCycleSet(g_timer0_pwm_2.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,A_OUTPUT);
            break;
        case PWM3:
            ssp_err = g_timer9_pwm_3.p_api->dutyCycleSet(g_timer9_pwm_3.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,A_OUTPUT);
            break;
        case PWM4:
            ssp_err = g_timer10_pwm_4.p_api->dutyCycleSet(g_timer10_pwm_4.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,A_OUTPUT);
            break;
        default:
            break;
    }
    if (ssp_err == SSP_SUCCESS)
    {
        //db_vt_set_pwm_output(pwm_id, pwm_value); // update database
        status = SUCCESS;
    }
    else
    {
        status = FAILURE;
    }
    return(status);
}

//--------------------------------------------------------------------------------
// NAME      : hw_set_relay
// ABSTRACT  : This function sets the state of the relays without the zero cross
// signal.
// ARGUMENTS :
//  RELAY_ID_E relay_id         - the id of the relay 1,2, or 3
//  RELAY_STATE_E relay_state   - the desired state of the relay OPEN or CLOSE
// RETURN    :
//--------------------------------------------------------------------------------
void hw_set_relay(RELAY_ID_E relay_id, RELAY_STATE_E relay_state)
{
    switch(relay_id)
    {
    case RELAY_1:
        if (relay_state == CLOSE)
            g_ioport.p_api->pinWrite(RELAY1, IOPORT_LEVEL_HIGH);
        else
            g_ioport.p_api->pinWrite(RELAY1, IOPORT_LEVEL_LOW);
        break;
    case RELAY_2:
        if (relay_state == CLOSE)
            g_ioport.p_api->pinWrite(RELAY2, IOPORT_LEVEL_HIGH);
        else
            g_ioport.p_api->pinWrite(RELAY2, IOPORT_LEVEL_LOW);
        break;
    case RELAY_3:
        if (relay_state == CLOSE)
        {
            g_ioport.p_api->pinWrite(RELAY3_SET, IOPORT_LEVEL_HIGH);
            // DELAY FOR 15 MILLISECOND
            R_BSP_SoftwareDelay(15, BSP_DELAY_UNITS_MILLISECONDS);
            //
            g_ioport.p_api->pinWrite(RELAY3_SET, IOPORT_LEVEL_LOW);
        }
        else
        {
            g_ioport.p_api->pinWrite(RELAY3_RESET, IOPORT_LEVEL_HIGH);
            // DELAY FOR 15 MILLISECOND
            R_BSP_SoftwareDelay(15, BSP_DELAY_UNITS_MILLISECONDS);
            //
            g_ioport.p_api->pinWrite(RELAY3_RESET, IOPORT_LEVEL_LOW);
        }
        break;
    case PHASE_RELAY:
        if (relay_state == CLOSE)
            g_ioport.p_api->pinWrite(RELAYP, IOPORT_LEVEL_HIGH);
        else
            g_ioport.p_api->pinWrite(RELAYP, IOPORT_LEVEL_LOW);
        break;
    }
}

//--------------------------------------------------------------------------------
// NAME      : hw_set_analog_switch
// ABSTRACT  : This function sets the analog switch state signal.
//
// ARGUMENTS :
//  ANALOG_SW_E analog_switch_sel           - analog_switch_sel ANALOG_SW_AI3, AI4
//  AN_SWITCH_STATE_E analog_switch_state   - analog_switch_state SW_CLOSE, SW_OPEN
// RETURN    :
//--------------------------------------------------------------------------------
void hw_set_analog_switch(ANALOG_SW_E analog_switch_sel, AN_SWITCH_STATE_E analog_switch_state)
{
    switch (analog_switch_sel)
    {
    case ANALOG_SW_AI3:
        if (analog_switch_state == SW_CLOSE)
            g_ioport.p_api->pinWrite(I_AI3, IOPORT_LEVEL_HIGH);
        else
            g_ioport.p_api->pinWrite(I_AI3, IOPORT_LEVEL_LOW);
        break;
    case ANALOG_SW_AI4:
        if (analog_switch_state == SW_CLOSE)
            g_ioport.p_api->pinWrite(I_AI4, IOPORT_LEVEL_HIGH);
        else
            g_ioport.p_api->pinWrite(I_AI4, IOPORT_LEVEL_LOW);
        break;

    }
}

//--------------------------------------------------------------------------------
// NAME      : hw_set_phase_gate_enable
// ABSTRACT  : This function sets the phase gate enable.
//
// ARGUMENTS :
//  PHASE_GATE_E phase_enable           - phase_enable PHASE_GATE_ENABLE, DISABLE
// RETURN    :
//--------------------------------------------------------------------------------
void hw_set_phase_gate_enable(PHASE_GATE_E phase_enable)
{
    if (phase_enable == PHASE_GATE_ENABLE)
        g_ioport.p_api->pinWrite(PHASE_GATE_EN, IOPORT_LEVEL_HIGH);
    else
        g_ioport.p_api->pinWrite(PHASE_GATE_EN, IOPORT_LEVEL_LOW);
}
