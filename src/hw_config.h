//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: hw_config.h contains defines for the specific board (MEG board)
//  being used
//
//-----------------------------------------------------------------------------
#ifndef _HW_CONFIG_H_ //
#define _HW_CONFIG_H_ //

// Hardware Configuration file for MEG board

//
// Digital Inputs
//
#define DIGIN1          IOPORT_PORT_03_PIN_11 // Digital input 1
#define DIGIN2          IOPORT_PORT_03_PIN_12 // Digital input 2
#define DIGIN3          IOPORT_PORT_02_PIN_00 // Digital input 3
#define DIGIN4          IOPORT_PORT_02_PIN_08 // Digital input 4

//
// LED display
//
#define LATCH_A2        IOPORT_PORT_02_PIN_14 // LED driver latch address bit 2
#define LATCH_A1        IOPORT_PORT_02_PIN_11 // LED driver latch address bit 1
#define LATCH_A0        IOPORT_PORT_02_PIN_10 // LED driver latch address bit 0

#define LED_DATA        IOPORT_PORT_02_PIN_09 // LED driver data to latch

#define LATCH_EN1       IOPORT_PORT_08_PIN_00 // LED driver latch 1 enable
#define LATCH_MR1       IOPORT_PORT_08_PIN_01 // LED driver latch 1 memory reset

#define LATCH_EN2       IOPORT_PORT_04_PIN_00 // LED driver latch 2 enable
#define LATCH_MR2       IOPORT_PORT_04_PIN_13 // LED driver latch 2 memory reset

#define LATCH_EN3       IOPORT_PORT_04_PIN_12 // LED driver latch 3 enable
#define LATCH_MR3       IOPORT_PORT_04_PIN_10 // LED driver latch 3 memory reset

//
// Relays
//
#define RELAY1          IOPORT_PORT_07_PIN_12 // Relay 1 control
#define RELAY2          IOPORT_PORT_07_PIN_10 // Relay 2 control

#define RELAY3_SET      IOPORT_PORT_04_PIN_03 // Latching Relay 3 SET
#define RELAY3_RESET    IOPORT_PORT_04_PIN_04 // Latching Relay 3 RESET

#define RELAYP          IOPORT_PORT_02_PIN_12 // Phase Relay control

//
// ZC - zero cross
//
#define R1_ZC_EN        IOPORT_PORT_05_PIN_00 // Enables switch for zero cross signal on Relay 1
#define R2_ZC_EN        IOPORT_PORT_05_PIN_03 // Enables switch for zero cross signal on Relay 2
#define R3_ZC_EN        IOPORT_PORT_05_PIN_04 // Enables switch for zero cross signal on Relay 3

//
// dimming
//
#define PHASE_GATE_EN   IOPORT_PORT_05_PIN_08 // Gate drive signal for phase dimming MOSFET output

//
//
//
#define I_AI4           IOPORT_PORT_00_PIN_15 //
#define I_AI3           IOPORT_PORT_00_PIN_14 //

//
// BT
//
#define BT_RESET        IOPORT_PORT_06_PIN_08 // BT module reset
#define BT_AT_CMD_MODE  IOPORT_PORT_06_PIN_09 // Selects BT Module AT command mode
#define BT_BEACON_MODE  IOPORT_PORT_06_PIN_10 // Selects BT Module beacon mode

//
// WIFI
//
#define WIFI_GPIO4      IOPORT_PORT_03_PIN_10 //
#define WIFI_GPIO1      IOPORT_PORT_03_PIN_09 //
#define WIFI_RESET      IOPORT_PORT_03_PIN_08 //
#define WIFI_FAC_RST    IOPORT_PORT_03_PIN_07 //
#define WIFI_CHIP_EN    IOPORT_PORT_03_PIN_06 //
#define WIFI_WAKE       IOPORT_PORT_03_PIN_03 //

#endif // _HW_CONFIG_H_
