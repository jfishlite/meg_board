//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: watchdog_driver.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __WATCHDOG_DRIVER_H__
#define __WATCHDOG_DRIVER_H__

void init_watchdog_driver(void);

void start_restart_watchdog(void);

#endif //__WATCHDOG_DRIVER_H__
