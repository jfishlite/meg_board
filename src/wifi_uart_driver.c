#include "wifi_uart_driver.h"
#include "wifi_thread.h"

void init_wifi_uart(void)
{
    // open and initialize the serial port
    wifi_uart2.p_api->open(wifi_uart2.p_ctrl, wifi_uart2.p_cfg);

}

void wifi_uart2_callback(uart_callback_args_t *p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
