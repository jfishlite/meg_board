/* generated HAL header file - do not edit */
#ifndef HAL_DATA_H_
#define HAL_DATA_H_
#include <stdint.h>
#include "bsp_api.h"
#include "common_data.h"
#include "r_kint.h"
#include "r_keymatrix_api.h"
#include "r_wdt.h"
#include "r_wdt_api.h"
#include "r_dtc.h"
#include "r_transfer_api.h"
#include "r_riic.h"
#include "r_i2c_api.h"
#ifdef __cplusplus
extern "C" {
#endif
/** Key Matrix on KINT Instance. */
extern const keymatrix_instance_t g_kint0;
#ifdef pushbutton_interrupt
#define KEYMATRIX_ON_KINT_CALLBACK_USED_g_kint0 (0)
#else
#define KEYMATRIX_ON_KINT_CALLBACK_USED_g_kint0 (1)
#endif
#if KEYMATRIX_ON_KINT_CALLBACK_USED_g_kint0
void pushbutton_interrupt(keymatrix_callback_args_t *p_args);
#endif
#ifndef pushbutton_interrupt
void pushbutton_interrupt(keymatrix_callback_args_t *p_args);
#endif
/** WDT on WDT Instance. */
extern const wdt_instance_t g_wdt0;
#ifndef watchdog_callback
void watchdog_callback(wdt_callback_args_t *p_args);
#endif
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer9;
#ifndef NULL
void NULL(transfer_callback_args_t *p_args);
#endif
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer8;
#ifndef NULL
void NULL(transfer_callback_args_t *p_args);
#endif
extern const i2c_cfg_t rtc_i2c0_cfg;
/** I2C on RIIC Instance. */
extern const i2c_master_instance_t rtc_i2c0;
#ifndef rtc_callback
void rtc_callback(i2c_callback_args_t *p_args);
#endif

extern riic_instance_ctrl_t rtc_i2c0_ctrl;
extern const riic_extended_cfg rtc_i2c0_extend;
#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == g_transfer8)
#define rtc_i2c0_P_TRANSFER_TX (NULL)
#else
#define rtc_i2c0_P_TRANSFER_TX (&g_transfer8)
#endif
#if (SYNERGY_NOT_DEFINED == g_transfer9)
#define rtc_i2c0_P_TRANSFER_RX (NULL)
#else
#define rtc_i2c0_P_TRANSFER_RX (&g_transfer9)
#endif
#undef SYNERGY_NOT_DEFINED
#define rtc_i2c0_P_EXTEND (&rtc_i2c0_extend)
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer11;
#ifndef NULL
void NULL(transfer_callback_args_t *p_args);
#endif
/* Transfer on DTC Instance. */
extern const transfer_instance_t g_transfer10;
#ifndef NULL
void NULL(transfer_callback_args_t *p_args);
#endif
extern const i2c_cfg_t mac_i2c0_cfg;
/** I2C on RIIC Instance. */
extern const i2c_master_instance_t mac_i2c0;
#ifndef mac_callback
void mac_callback(i2c_callback_args_t *p_args);
#endif

extern riic_instance_ctrl_t mac_i2c0_ctrl;
extern const riic_extended_cfg mac_i2c0_extend;
#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == g_transfer10)
#define mac_i2c0_P_TRANSFER_TX (NULL)
#else
#define mac_i2c0_P_TRANSFER_TX (&g_transfer10)
#endif
#if (SYNERGY_NOT_DEFINED == g_transfer11)
#define mac_i2c0_P_TRANSFER_RX (NULL)
#else
#define mac_i2c0_P_TRANSFER_RX (&g_transfer11)
#endif
#undef SYNERGY_NOT_DEFINED
#define mac_i2c0_P_EXTEND (&mac_i2c0_extend)
void hal_entry(void);
void g_hal_init(void);
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* HAL_DATA_H_ */
