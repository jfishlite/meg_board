/* generated thread source file - do not edit */
#include "bt_bridge_thread.h"

TX_THREAD bt_bridge_thread;
void bt_bridge_thread_create(void);
static void bt_bridge_thread_func(ULONG thread_input);
static uint8_t bt_bridge_thread_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.bt_bridge_thread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer7) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_SCI0_RXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_SCI0_RXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer7_ctrl;
transfer_info_t g_transfer7_info = { .dest_addr_mode =
        TRANSFER_ADDR_MODE_INCREMENTED, .repeat_area =
        TRANSFER_REPEAT_AREA_DESTINATION, .irq = TRANSFER_IRQ_END, .chain_mode =
        TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_FIXED,
        .size = TRANSFER_SIZE_1_BYTE, .mode = TRANSFER_MODE_NORMAL, .p_dest =
                (void *) NULL, .p_src = (void const *) NULL, .num_blocks = 0,
        .length = 0, };
const transfer_cfg_t g_transfer7_cfg = { .p_info = &g_transfer7_info,
        .activation_source = ELC_EVENT_SCI0_RXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer7, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer7 = { .p_ctrl = &g_transfer7_ctrl, .p_cfg =
        &g_transfer7_cfg, .p_api = &g_transfer_on_dtc };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer6) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_SCI0_TXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_SCI0_TXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer6_ctrl;
transfer_info_t g_transfer6_info = { .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED,
        .repeat_area = TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
        .chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode =
                TRANSFER_ADDR_MODE_INCREMENTED, .size = TRANSFER_SIZE_1_BYTE,
        .mode = TRANSFER_MODE_NORMAL, .p_dest = (void *) NULL, .p_src =
                (void const *) NULL, .num_blocks = 0, .length = 0, };
const transfer_cfg_t g_transfer6_cfg = { .p_info = &g_transfer6_info,
        .activation_source = ELC_EVENT_SCI0_TXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer6, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer6 = { .p_ctrl = &g_transfer6_ctrl, .p_cfg =
        &g_transfer6_cfg, .p_api = &g_transfer_on_dtc };
#if SCI_UART_CFG_RX_ENABLE
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_bt_bridge_uart9) && !defined(SSP_SUPPRESS_ISR_SCI0)
SSP_VECTOR_DEFINE_CHAN(sci_uart_rxi_isr, SCI, RXI, 0);
#endif
#endif
#endif
#if SCI_UART_CFG_TX_ENABLE
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_bt_bridge_uart9) && !defined(SSP_SUPPRESS_ISR_SCI0)
SSP_VECTOR_DEFINE_CHAN(sci_uart_txi_isr, SCI, TXI, 0);
#endif
#endif
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_bt_bridge_uart9) && !defined(SSP_SUPPRESS_ISR_SCI0)
SSP_VECTOR_DEFINE_CHAN(sci_uart_tei_isr, SCI, TEI, 0);
#endif
#endif
#endif
#if SCI_UART_CFG_RX_ENABLE
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_bt_bridge_uart9) && !defined(SSP_SUPPRESS_ISR_SCI0)
SSP_VECTOR_DEFINE_CHAN(sci_uart_eri_isr, SCI, ERI, 0);
#endif
#endif
#endif
sci_uart_instance_ctrl_t bt_bridge_uart9_ctrl;

/** UART extended configuration for UARTonSCI HAL driver */
const uart_on_sci_cfg_t bt_bridge_uart9_cfg_extend = { .clk_src =
        SCI_CLK_SRC_INT, .baudclk_out = false, .rx_edge_start = true,
        .noisecancel_en = false, .p_extpin_ctrl = NULL, .bitrate_modulation =
                true, .rx_fifo_trigger = SCI_UART_RX_FIFO_TRIGGER_MAX };

/** UART interface configuration */
const uart_cfg_t bt_bridge_uart9_cfg = { .channel = 0, .baud_rate = 115200,
        .data_bits = UART_DATA_BITS_8, .parity = UART_PARITY_OFF, .stop_bits =
                UART_STOP_BITS_1, .ctsrts_en = false, .p_callback =
                bt_bridge_uart_callback, .p_context = &bt_bridge_uart9,
        .p_extend = &bt_bridge_uart9_cfg_extend,
#define SYNERGY_NOT_DEFINED (1)                        
#if (SYNERGY_NOT_DEFINED == g_transfer6)
        .p_transfer_tx = NULL,
#else
        .p_transfer_tx = &g_transfer6,
#endif            
#if (SYNERGY_NOT_DEFINED == g_transfer7)
        .p_transfer_rx = NULL,
#else
        .p_transfer_rx = &g_transfer7,
#endif   
#undef SYNERGY_NOT_DEFINED            
        .rxi_ipl = (12), .txi_ipl = (12), .tei_ipl = (12), .eri_ipl =
                (BSP_IRQ_DISABLED), };

/* Instance structure to use this module. */
const uart_instance_t bt_bridge_uart9 = { .p_ctrl = &bt_bridge_uart9_ctrl,
        .p_cfg = &bt_bridge_uart9_cfg, .p_api = &g_uart_on_sci };
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void bt_bridge_thread_create(void) {
    /* Increment count so we will know the number of ISDE created threads. */
    g_ssp_common_thread_count++;

    /* Initialize each kernel object. */

    UINT err;
    err = tx_thread_create(&bt_bridge_thread, (CHAR *) "bt bridge thread",
            bt_bridge_thread_func, (ULONG) NULL, &bt_bridge_thread_stack, 1024,
            1, 1, 1, TX_AUTO_START);
    if (TX_SUCCESS != err) {
        tx_startup_err_callback(&bt_bridge_thread, 0);
    }
}

static void bt_bridge_thread_func(ULONG thread_input) {
    /* Not currently using thread_input. */
    SSP_PARAMETER_NOT_USED(thread_input);

    /* Initialize common components */
    tx_startup_common_init();

    /* Initialize each module instance. */

    /* Enter user code for this thread. */
    bt_bridge_thread_entry();
}
