/* generated thread source file - do not edit */
#include "output_thread.h"

TX_THREAD output_thread;
void output_thread_create(void);
static void output_thread_func(ULONG thread_input);
static uint8_t output_thread_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.output_thread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_et_external_irq9) && !defined(SSP_SUPPRESS_ISR_ICU9)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ9);
#endif
#endif
static icu_instance_ctrl_t et_external_irq9_ctrl;
static const external_irq_cfg_t et_external_irq9_cfg = { .channel = 9,
        .trigger = EXTERNAL_IRQ_TRIG_RISING, .filter_enable = false, .pclk_div =
                EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true, .p_callback =
                et_irq9, .p_context = &et_external_irq9, .p_extend = NULL,
        .irq_ipl = (12), };
/* Instance structure to use this module. */
const external_irq_instance_t et_external_irq9 = { .p_ctrl =
        &et_external_irq9_ctrl, .p_cfg = &et_external_irq9_cfg, .p_api =
        &g_external_irq_on_icu };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer10_pwm_4) && !defined(SSP_SUPPRESS_ISR_GPT10)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 10);
#endif
#endif
static gpt_instance_ctrl_t g_timer10_pwm_4_ctrl;
static const timer_on_gpt_cfg_t g_timer10_pwm_4_extend = { .gtioca = {
        .output_enabled = true, .stop_level = GPT_PIN_LEVEL_RETAINED },
        .gtiocb = { .output_enabled = false, .stop_level =
                GPT_PIN_LEVEL_RETAINED }, .shortest_pwm_signal =
                GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer10_pwm_4_cfg = { .mode = TIMER_MODE_PWM,
        .period = 25, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50,
        .duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 10,
        .autostart = true, .p_callback = pwm4_callback, .p_context =
                &g_timer10_pwm_4, .p_extend = &g_timer10_pwm_4_extend,
        .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer10_pwm_4 = { .p_ctrl = &g_timer10_pwm_4_ctrl,
        .p_cfg = &g_timer10_pwm_4_cfg, .p_api = &g_timer_on_gpt };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer9_pwm_3) && !defined(SSP_SUPPRESS_ISR_GPT9)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 9);
#endif
#endif
static gpt_instance_ctrl_t g_timer9_pwm_3_ctrl;
static const timer_on_gpt_cfg_t g_timer9_pwm_3_extend = { .gtioca = {
        .output_enabled = true, .stop_level = GPT_PIN_LEVEL_RETAINED },
        .gtiocb = { .output_enabled = false, .stop_level =
                GPT_PIN_LEVEL_RETAINED }, .shortest_pwm_signal =
                GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer9_pwm_3_cfg = { .mode = TIMER_MODE_PWM,
        .period = 25, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50,
        .duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 9, .autostart =
                true, .p_callback = pwm3_callback, .p_context = &g_timer9_pwm_3,
        .p_extend = &g_timer9_pwm_3_extend, .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer9_pwm_3 = { .p_ctrl = &g_timer9_pwm_3_ctrl,
        .p_cfg = &g_timer9_pwm_3_cfg, .p_api = &g_timer_on_gpt };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer2_pwm_1) && !defined(SSP_SUPPRESS_ISR_GPT2)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 2);
#endif
#endif
static gpt_instance_ctrl_t g_timer2_pwm_1_ctrl;
static const timer_on_gpt_cfg_t g_timer2_pwm_1_extend = { .gtioca = {
        .output_enabled = true, .stop_level = GPT_PIN_LEVEL_RETAINED },
        .gtiocb = { .output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW },
        .shortest_pwm_signal = GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer2_pwm_1_cfg = { .mode = TIMER_MODE_PWM,
        .period = 25, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50,
        .duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 2, .autostart =
                true, .p_callback = pwm1_callback, .p_context = &g_timer2_pwm_1,
        .p_extend = &g_timer2_pwm_1_extend, .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer2_pwm_1 = { .p_ctrl = &g_timer2_pwm_1_ctrl,
        .p_cfg = &g_timer2_pwm_1_cfg, .p_api = &g_timer_on_gpt };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer0_pwm_2) && !defined(SSP_SUPPRESS_ISR_GPT0)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 0);
#endif
#endif
static gpt_instance_ctrl_t g_timer0_pwm_2_ctrl;
static const timer_on_gpt_cfg_t g_timer0_pwm_2_extend = { .gtioca = {
        .output_enabled = true, .stop_level = GPT_PIN_LEVEL_RETAINED },
        .gtiocb = { .output_enabled = false, .stop_level =
                GPT_PIN_LEVEL_RETAINED }, .shortest_pwm_signal =
                GPT_SHORTEST_LEVEL_OFF, };
static const timer_cfg_t g_timer0_pwm_2_cfg = { .mode = TIMER_MODE_PWM,
        .period = 25, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50,
        .duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 0, .autostart =
                true, .p_callback = pwm2_callback, .p_context = &g_timer0_pwm_2,
        .p_extend = &g_timer0_pwm_2_extend, .irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer0_pwm_2 = { .p_ctrl = &g_timer0_pwm_2_ctrl,
        .p_cfg = &g_timer0_pwm_2_cfg, .p_api = &g_timer_on_gpt };
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq15) && !defined(SSP_SUPPRESS_ISR_ICU15)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ15);
#endif
#endif
static icu_instance_ctrl_t g_external_irq15_ctrl;
static const external_irq_cfg_t g_external_irq15_cfg = { .channel = 15,
        .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false,
        .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true,
        .p_callback = irq15_capture_phase_zc, .p_context = &g_external_irq15,
        .p_extend = NULL, .irq_ipl = (12), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq15 = { .p_ctrl =
        &g_external_irq15_ctrl, .p_cfg = &g_external_irq15_cfg, .p_api =
        &g_external_irq_on_icu };
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq14) && !defined(SSP_SUPPRESS_ISR_ICU14)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ14);
#endif
#endif
static icu_instance_ctrl_t g_external_irq14_ctrl;
static const external_irq_cfg_t g_external_irq14_cfg = { .channel = 14,
        .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false,
        .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true,
        .p_callback = irq14_capture_zc3, .p_context = &g_external_irq14,
        .p_extend = NULL, .irq_ipl = (12), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq14 = { .p_ctrl =
        &g_external_irq14_ctrl, .p_cfg = &g_external_irq14_cfg, .p_api =
        &g_external_irq_on_icu };
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq12) && !defined(SSP_SUPPRESS_ISR_ICU12)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ12);
#endif
#endif
static icu_instance_ctrl_t g_external_irq12_ctrl;
static const external_irq_cfg_t g_external_irq12_cfg = { .channel = 12,
        .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false,
        .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true,
        .p_callback = irq12_capture_zc2, .p_context = &g_external_irq12,
        .p_extend = NULL, .irq_ipl = (12), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq12 = { .p_ctrl =
        &g_external_irq12_ctrl, .p_cfg = &g_external_irq12_cfg, .p_api =
        &g_external_irq_on_icu };
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq11) && !defined(SSP_SUPPRESS_ISR_ICU11)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ11);
#endif
#endif
static icu_instance_ctrl_t g_external_irq11_ctrl;
static const external_irq_cfg_t g_external_irq11_cfg = { .channel = 11,
        .trigger = EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false,
        .pclk_div = EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true,
        .p_callback = irq11_capture_zc1, .p_context = &g_external_irq11,
        .p_extend = NULL, .irq_ipl = (12), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq11 = { .p_ctrl =
        &g_external_irq11_ctrl, .p_cfg = &g_external_irq11_cfg, .p_api =
        &g_external_irq_on_icu };
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void output_thread_create(void) {
    /* Increment count so we will know the number of ISDE created threads. */
    g_ssp_common_thread_count++;

    /* Initialize each kernel object. */

    UINT err;
    err = tx_thread_create(&output_thread, (CHAR *) "output thread",
            output_thread_func, (ULONG) NULL, &output_thread_stack, 1024, 1, 1,
            1, TX_AUTO_START);
    if (TX_SUCCESS != err) {
        tx_startup_err_callback(&output_thread, 0);
    }
}

static void output_thread_func(ULONG thread_input) {
    /* Not currently using thread_input. */
    SSP_PARAMETER_NOT_USED(thread_input);

    /* Initialize common components */
    tx_startup_common_init();

    /* Initialize each module instance. */

    /* Enter user code for this thread. */
    output_thread_entry();
}
