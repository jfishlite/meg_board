/* generated thread source file - do not edit */
#include "bt_thread.h"

TX_THREAD bt_thread;
void bt_thread_create(void);
static void bt_thread_func(ULONG thread_input);
static uint8_t bt_thread_stack[1024] BSP_PLACE_IN_SECTION_V2(".stack.bt_thread") BSP_ALIGN_VARIABLE_V2(BSP_STACK_ALIGNMENT);
void tx_startup_err_callback(void *p_instance, void *p_data);
void tx_startup_common_init(void);
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer5) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_SCI7_RXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_SCI7_RXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer5_ctrl;
transfer_info_t g_transfer5_info = { .dest_addr_mode =
        TRANSFER_ADDR_MODE_INCREMENTED, .repeat_area =
        TRANSFER_REPEAT_AREA_DESTINATION, .irq = TRANSFER_IRQ_END, .chain_mode =
        TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_FIXED,
        .size = TRANSFER_SIZE_1_BYTE, .mode = TRANSFER_MODE_NORMAL, .p_dest =
                (void *) NULL, .p_src = (void const *) NULL, .num_blocks = 0,
        .length = 0, };
const transfer_cfg_t g_transfer5_cfg = { .p_info = &g_transfer5_info,
        .activation_source = ELC_EVENT_SCI7_RXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer5, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer5 = { .p_ctrl = &g_transfer5_ctrl, .p_cfg =
        &g_transfer5_cfg, .p_api = &g_transfer_on_dtc };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer4) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_SCI7_TXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_SCI7_TXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer4_ctrl;
transfer_info_t g_transfer4_info = { .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED,
        .repeat_area = TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
        .chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode =
                TRANSFER_ADDR_MODE_INCREMENTED, .size = TRANSFER_SIZE_1_BYTE,
        .mode = TRANSFER_MODE_NORMAL, .p_dest = (void *) NULL, .p_src =
                (void const *) NULL, .num_blocks = 0, .length = 0, };
const transfer_cfg_t g_transfer4_cfg = { .p_info = &g_transfer4_info,
        .activation_source = ELC_EVENT_SCI7_TXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer4, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer4 = { .p_ctrl = &g_transfer4_ctrl, .p_cfg =
        &g_transfer4_cfg, .p_api = &g_transfer_on_dtc };
#if SCI_UART_CFG_RX_ENABLE
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_btm_dtm_uart7) && !defined(SSP_SUPPRESS_ISR_SCI7)
SSP_VECTOR_DEFINE_CHAN(sci_uart_rxi_isr, SCI, RXI, 7);
#endif
#endif
#endif
#if SCI_UART_CFG_TX_ENABLE
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_btm_dtm_uart7) && !defined(SSP_SUPPRESS_ISR_SCI7)
SSP_VECTOR_DEFINE_CHAN(sci_uart_txi_isr, SCI, TXI, 7);
#endif
#endif
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_btm_dtm_uart7) && !defined(SSP_SUPPRESS_ISR_SCI7)
SSP_VECTOR_DEFINE_CHAN(sci_uart_tei_isr, SCI, TEI, 7);
#endif
#endif
#endif
#if SCI_UART_CFG_RX_ENABLE
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_btm_dtm_uart7) && !defined(SSP_SUPPRESS_ISR_SCI7)
SSP_VECTOR_DEFINE_CHAN(sci_uart_eri_isr, SCI, ERI, 7);
#endif
#endif
#endif
sci_uart_instance_ctrl_t btm_dtm_uart7_ctrl;

/** UART extended configuration for UARTonSCI HAL driver */
const uart_on_sci_cfg_t btm_dtm_uart7_cfg_extend = { .clk_src = SCI_CLK_SRC_INT,
        .baudclk_out = false, .rx_edge_start = true, .noisecancel_en = false,
        .p_extpin_ctrl = NULL, .bitrate_modulation = true, .rx_fifo_trigger =
                SCI_UART_RX_FIFO_TRIGGER_MAX };

/** UART interface configuration */
const uart_cfg_t btm_dtm_uart7_cfg = { .channel = 7, .baud_rate = 115200,
        .data_bits = UART_DATA_BITS_8, .parity = UART_PARITY_OFF, .stop_bits =
                UART_STOP_BITS_1, .ctsrts_en = false, .p_callback =
                bt_dtm_uart_callback, .p_context = &btm_dtm_uart7, .p_extend =
                &btm_dtm_uart7_cfg_extend,
#define SYNERGY_NOT_DEFINED (1)                        
#if (SYNERGY_NOT_DEFINED == g_transfer4)
        .p_transfer_tx = NULL,
#else
        .p_transfer_tx = &g_transfer4,
#endif            
#if (SYNERGY_NOT_DEFINED == g_transfer5)
        .p_transfer_rx = NULL,
#else
        .p_transfer_rx = &g_transfer5,
#endif   
#undef SYNERGY_NOT_DEFINED            
        .rxi_ipl = (12), .txi_ipl = (12), .tei_ipl = (12), .eri_ipl =
                (BSP_IRQ_DISABLED), };

/* Instance structure to use this module. */
const uart_instance_t btm_dtm_uart7 = { .p_ctrl = &btm_dtm_uart7_ctrl, .p_cfg =
        &btm_dtm_uart7_cfg, .p_api = &g_uart_on_sci };
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;

void bt_thread_create(void) {
    /* Increment count so we will know the number of ISDE created threads. */
    g_ssp_common_thread_count++;

    /* Initialize each kernel object. */

    UINT err;
    err = tx_thread_create(&bt_thread, (CHAR *) "bt thread", bt_thread_func,
            (ULONG) NULL, &bt_thread_stack, 1024, 1, 1, 1, TX_AUTO_START);
    if (TX_SUCCESS != err) {
        tx_startup_err_callback(&bt_thread, 0);
    }
}

static void bt_thread_func(ULONG thread_input) {
    /* Not currently using thread_input. */
    SSP_PARAMETER_NOT_USED(thread_input);

    /* Initialize common components */
    tx_startup_common_init();

    /* Initialize each module instance. */

    /* Enter user code for this thread. */
    bt_thread_entry();
}
