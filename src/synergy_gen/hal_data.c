/* generated HAL source file - do not edit */
#include "hal_data.h"
#if (12) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_kint0) && !defined(SSP_SUPPRESS_ISR_KINT)
SSP_VECTOR_DEFINE( key_int_isr, KEY, INT);
#endif
#endif
kint_instance_ctrl_t g_kint0_ctrl;
const keymatrix_cfg_t g_kint0_cfg = { .channels = (uint32_t)(
        ((uint32_t) 0x01) | ((uint32_t) 0x02) | ((uint32_t) 0x04)
                | ((uint32_t) 0x08) | ((uint32_t) 0x10) | ((uint32_t) 0x20)
                | ((uint32_t) 0x40) | ((uint32_t) 0x80)), .trigger =
        KEYMATRIX_TRIG_FALLING, .autostart = true, .p_callback =
        pushbutton_interrupt, .p_context = &g_kint0, .irq_ipl = (12), };
/* Instance structure to use this module. */
const keymatrix_instance_t g_kint0 = { .p_ctrl = &g_kint0_ctrl, .p_cfg =
        &g_kint0_cfg, .p_api = &g_keymatrix_on_kint };
static wdt_instance_ctrl_t g_wdt0_ctrl;

static const wdt_cfg_t g_wdt0_cfg =
        { .start_mode = WDT_START_MODE_REGISTER, .autostart = false, .timeout =
                WDT_TIMEOUT_16384, .clock_division = WDT_CLOCK_DIVISION_8192,
                .window_start = WDT_WINDOW_START_100, .window_end =
                        WDT_WINDOW_END_0,
                .reset_control = WDT_RESET_CONTROL_NMI, .stop_control =
                        WDT_STOP_CONTROL_ENABLE, .p_callback = watchdog_callback, };

/* Instance structure to use this module. */
const wdt_instance_t g_wdt0 = { .p_ctrl = &g_wdt0_ctrl, .p_cfg = &g_wdt0_cfg,
        .p_api = &g_wdt_on_wdt };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer9) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_IIC2_RXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_IIC2_RXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer9_ctrl;
transfer_info_t g_transfer9_info = { .dest_addr_mode =
        TRANSFER_ADDR_MODE_INCREMENTED, .repeat_area =
        TRANSFER_REPEAT_AREA_DESTINATION, .irq = TRANSFER_IRQ_END, .chain_mode =
        TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_FIXED,
        .size = TRANSFER_SIZE_1_BYTE, .mode = TRANSFER_MODE_NORMAL, .p_dest =
                (void *) NULL, .p_src = (void const *) NULL, .num_blocks = 0,
        .length = 0, };
const transfer_cfg_t g_transfer9_cfg = { .p_info = &g_transfer9_info,
        .activation_source = ELC_EVENT_IIC2_RXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer9, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer9 = { .p_ctrl = &g_transfer9_ctrl, .p_cfg =
        &g_transfer9_cfg, .p_api = &g_transfer_on_dtc };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer8) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_IIC2_TXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_IIC2_TXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer8_ctrl;
transfer_info_t g_transfer8_info = { .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED,
        .repeat_area = TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
        .chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode =
                TRANSFER_ADDR_MODE_INCREMENTED, .size = TRANSFER_SIZE_1_BYTE,
        .mode = TRANSFER_MODE_NORMAL, .p_dest = (void *) NULL, .p_src =
                (void const *) NULL, .num_blocks = 0, .length = 0, };
const transfer_cfg_t g_transfer8_cfg = { .p_info = &g_transfer8_info,
        .activation_source = ELC_EVENT_IIC2_TXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer8, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer8 = { .p_ctrl = &g_transfer8_ctrl, .p_cfg =
        &g_transfer8_cfg, .p_api = &g_transfer_on_dtc };
#if !defined(SSP_SUPPRESS_ISR_rtc_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC2)
SSP_VECTOR_DEFINE_CHAN(iic_rxi_isr, IIC, RXI, 2);
#endif
#if !defined(SSP_SUPPRESS_ISR_rtc_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC2)
SSP_VECTOR_DEFINE_CHAN(iic_txi_isr, IIC, TXI, 2);
#endif
#if !defined(SSP_SUPPRESS_ISR_rtc_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC2)
SSP_VECTOR_DEFINE_CHAN(iic_tei_isr, IIC, TEI, 2);
#endif
#if !defined(SSP_SUPPRESS_ISR_rtc_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC2)
SSP_VECTOR_DEFINE_CHAN(iic_eri_isr, IIC, ERI, 2);
#endif
riic_instance_ctrl_t rtc_i2c0_ctrl;
const riic_extended_cfg rtc_i2c0_extend = { .timeout_mode =
        RIIC_TIMEOUT_MODE_SHORT, };
const i2c_cfg_t rtc_i2c0_cfg = { .channel = 2, .rate = I2C_RATE_STANDARD,
        .slave = 0x00, .addr_mode = I2C_ADDR_MODE_7BIT,
#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == g_transfer8)
        .p_transfer_tx = NULL,
#else
        .p_transfer_tx = &g_transfer8,
#endif
#if (SYNERGY_NOT_DEFINED == g_transfer9)
        .p_transfer_rx = NULL,
#else
        .p_transfer_rx = &g_transfer9,
#endif
#undef SYNERGY_NOT_DEFINED	
        .p_callback = rtc_callback, .p_context = (void *) &rtc_i2c0, .rxi_ipl =
                (12), .txi_ipl = (12), .tei_ipl = (12), .eri_ipl = (12),
        .p_extend = &rtc_i2c0_extend, };
/* Instance structure to use this module. */
const i2c_master_instance_t rtc_i2c0 = { .p_ctrl = &rtc_i2c0_ctrl, .p_cfg =
        &rtc_i2c0_cfg, .p_api = &g_i2c_master_on_riic };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer11) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_IIC0_RXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_IIC0_RXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer11_ctrl;
transfer_info_t g_transfer11_info = { .dest_addr_mode =
        TRANSFER_ADDR_MODE_INCREMENTED, .repeat_area =
        TRANSFER_REPEAT_AREA_DESTINATION, .irq = TRANSFER_IRQ_END, .chain_mode =
        TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode = TRANSFER_ADDR_MODE_FIXED,
        .size = TRANSFER_SIZE_1_BYTE, .mode = TRANSFER_MODE_NORMAL, .p_dest =
                (void *) NULL, .p_src = (void const *) NULL, .num_blocks = 0,
        .length = 0, };
const transfer_cfg_t g_transfer11_cfg = { .p_info = &g_transfer11_info,
        .activation_source = ELC_EVENT_IIC0_RXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer11, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer11 = { .p_ctrl = &g_transfer11_ctrl,
        .p_cfg = &g_transfer11_cfg, .p_api = &g_transfer_on_dtc };
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_transfer10) && !defined(SSP_SUPPRESS_ISR_DTCELC_EVENT_IIC0_TXI)
#define DTC_ACTIVATION_SRC_ELC_EVENT_IIC0_TXI
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_0) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_0);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_0
#endif
#if defined(DTC_ACTIVATION_SRC_ELC_EVENT_ELC_SOFTWARE_EVENT_1) && !defined(DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1)
SSP_VECTOR_DEFINE(elc_software_event_isr, ELC, SOFTWARE_EVENT_1);
#define DTC_VECTOR_DEFINED_SOFTWARE_EVENT_1
#endif
#endif
#endif

dtc_instance_ctrl_t g_transfer10_ctrl;
transfer_info_t g_transfer10_info = {
        .dest_addr_mode = TRANSFER_ADDR_MODE_FIXED, .repeat_area =
                TRANSFER_REPEAT_AREA_SOURCE, .irq = TRANSFER_IRQ_END,
        .chain_mode = TRANSFER_CHAIN_MODE_DISABLED, .src_addr_mode =
                TRANSFER_ADDR_MODE_INCREMENTED, .size = TRANSFER_SIZE_1_BYTE,
        .mode = TRANSFER_MODE_NORMAL, .p_dest = (void *) NULL, .p_src =
                (void const *) NULL, .num_blocks = 0, .length = 0, };
const transfer_cfg_t g_transfer10_cfg = { .p_info = &g_transfer10_info,
        .activation_source = ELC_EVENT_IIC0_TXI, .auto_enable = false,
        .p_callback = NULL, .p_context = &g_transfer10, .irq_ipl =
                (BSP_IRQ_DISABLED) };
/* Instance structure to use this module. */
const transfer_instance_t g_transfer10 = { .p_ctrl = &g_transfer10_ctrl,
        .p_cfg = &g_transfer10_cfg, .p_api = &g_transfer_on_dtc };
#if !defined(SSP_SUPPRESS_ISR_mac_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC0)
SSP_VECTOR_DEFINE_CHAN(iic_rxi_isr, IIC, RXI, 0);
#endif
#if !defined(SSP_SUPPRESS_ISR_mac_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC0)
SSP_VECTOR_DEFINE_CHAN(iic_txi_isr, IIC, TXI, 0);
#endif
#if !defined(SSP_SUPPRESS_ISR_mac_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC0)
SSP_VECTOR_DEFINE_CHAN(iic_tei_isr, IIC, TEI, 0);
#endif
#if !defined(SSP_SUPPRESS_ISR_mac_i2c0) && !defined(SSP_SUPPRESS_ISR_IIC0)
SSP_VECTOR_DEFINE_CHAN(iic_eri_isr, IIC, ERI, 0);
#endif
riic_instance_ctrl_t mac_i2c0_ctrl;
const riic_extended_cfg mac_i2c0_extend = { .timeout_mode =
        RIIC_TIMEOUT_MODE_SHORT, };
const i2c_cfg_t mac_i2c0_cfg = { .channel = 0, .rate = I2C_RATE_STANDARD,
        .slave = 0x00, .addr_mode = I2C_ADDR_MODE_7BIT,
#define SYNERGY_NOT_DEFINED (1)            
#if (SYNERGY_NOT_DEFINED == g_transfer10)
        .p_transfer_tx = NULL,
#else
        .p_transfer_tx = &g_transfer10,
#endif
#if (SYNERGY_NOT_DEFINED == g_transfer11)
        .p_transfer_rx = NULL,
#else
        .p_transfer_rx = &g_transfer11,
#endif
#undef SYNERGY_NOT_DEFINED	
        .p_callback = mac_callback, .p_context = (void *) &mac_i2c0, .rxi_ipl =
                (12), .txi_ipl = (12), .tei_ipl = (12), .eri_ipl = (12),
        .p_extend = &mac_i2c0_extend, };
/* Instance structure to use this module. */
const i2c_master_instance_t mac_i2c0 = { .p_ctrl = &mac_i2c0_ctrl, .p_cfg =
        &mac_i2c0_cfg, .p_api = &g_i2c_master_on_riic };
void g_hal_init(void) {
    g_common_init();
}
