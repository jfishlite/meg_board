/* generated thread header file - do not edit */
#ifndef OUTPUT_THREAD_H_
#define OUTPUT_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus
extern "C" void output_thread_entry(void);
#else
extern void output_thread_entry(void);
#endif
#include "r_icu.h"
#include "r_external_irq_api.h"
#include "r_gpt.h"
#include "r_timer_api.h"
#ifdef __cplusplus
extern "C" {
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t et_external_irq9;
#ifndef et_irq9
void et_irq9(external_irq_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer10_pwm_4;
#ifndef pwm4_callback
void pwm4_callback(timer_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer9_pwm_3;
#ifndef pwm3_callback
void pwm3_callback(timer_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer2_pwm_1;
#ifndef pwm1_callback
void pwm1_callback(timer_callback_args_t *p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer0_pwm_2;
#ifndef pwm2_callback
void pwm2_callback(timer_callback_args_t *p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq15;
#ifndef irq15_capture_phase_zc
void irq15_capture_phase_zc(external_irq_callback_args_t *p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq14;
#ifndef irq14_capture_zc3
void irq14_capture_zc3(external_irq_callback_args_t *p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq12;
#ifndef irq12_capture_zc2
void irq12_capture_zc2(external_irq_callback_args_t *p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq11;
#ifndef irq11_capture_zc1
void irq11_capture_zc1(external_irq_callback_args_t *p_args);
#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* OUTPUT_THREAD_H_ */
