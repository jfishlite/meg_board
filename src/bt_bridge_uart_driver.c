#include "bt_bridge_uart_driver.h"
#include "bt_bridge_thread.h"


void init_bt_bridge_uart(void)
{
    // open and initialize the serial port
    bt_bridge_uart9.p_api->open(bt_bridge_uart9.p_ctrl, bt_bridge_uart9.p_cfg);

}


void bt_bridge_uart_callback(uart_callback_args_t *p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
