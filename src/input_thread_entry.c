//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: input_thread_entry - input_thread reads the adc channels
//  and digital inputs.
//
//-----------------------------------------------------------------------------
#include <input_thread.h>
#include "hw_config.h"

uint16_t pwr_n;

uint16_t analog1;
uint16_t analog2;
uint16_t analog3;
uint16_t analog4;

uint16_t current1;
uint16_t current2;
uint16_t current3;
uint16_t current4;

ioport_level_t digital_input1;
ioport_level_t digital_input2;
ioport_level_t digital_input3;
ioport_level_t digital_input4;


//--------------------------------------------------------------------------------
// NAME      : input_thread_entry
// ABSTRACT  :
//             input_thread_entry - input thread
// ARGUMENTS :
// RETURN    :
//--------------------------------------------------------------------------------
void input_thread_entry(void)
{
#if 0
    // scan a/d 0
    g_adc0.p_api->scanStart(g_adc0.p_ctrl);
    // scan a/d 1
    g_adc1.p_api->scanStart(g_adc1.p_ctrl);
#endif
    while (1)
    {
#if 0
        // see if adc0 scan is complete
        if (g_adc0.p_api->scanStatusGet(g_adc0.p_ctrl) == SSP_SUCCESS)
        {
                                                                            // pin#     peripheral      schematic
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_0, &pwr_n);   // P000     ADC0:AN00       PWR_IN
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_1, &analog1); // P001     ADC0:AN01       ANALOG1
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_2, &analog2); // P002     ADC0:AN02       ANALOG2
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_8, &analog3); // P008     ADC0:AN08       ANALOG3
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_9, &analog4); // P009     ADC0:AN09       ANALOG4

            // scan a/d 0
            g_adc0.p_api->scanStart(g_adc0.p_ctrl);
        }
        // see if adc1 scan is complete
        if (g_adc1.p_api->scanStatusGet(g_adc1.p_ctrl) == SSP_SUCCESS)
        {
                                                                             // pin#    peripheral     schematic
            g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_0, &current1); // P004    ADC1:AN00      CURRENT1
            g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_1, &current2); // P005    ADC1:AN01      CURRENT2
            g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_2, &current3); // P006    ADC1:AN02      CURRENT3
            g_adc1.p_api->read(g_adc1.p_ctrl, ADC_REG_CHANNEL_7, &current4); // P007    ADC1:AN07      CURRENT4

            // scan a/d 1
            g_adc1.p_api->scanStart(g_adc1.p_ctrl);
        }
        // read digital inputs
        g_ioport.p_api->pinRead(DIGIN1,&digital_input1);
        g_ioport.p_api->pinRead(DIGIN2,&digital_input2);
        g_ioport.p_api->pinRead(DIGIN3,&digital_input3);
        g_ioport.p_api->pinRead(DIGIN4,&digital_input4);
#endif
        tx_thread_sleep (1);
    }
}
