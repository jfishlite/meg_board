//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  relay_module.c
//
//  Description: Contains functions associated with:
//               - measuring line period
//               - controlling the ports to switch relays
//
//-----------------------------------------------------------------------------
#include "output_thread.h"

//--------------------------------------------------------------------------------
// NAME      : irq11_capture_zc1
// ABSTRACT  : This is a callback function for capturing line period
//
// ARGUMENTS : external_irq_callback_args_t * p_args
// RETURN    :
//--------------------------------------------------------------------------------
void irq11_capture_zc1(external_irq_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}

//--------------------------------------------------------------------------------
// NAME      : irq12_capture_zc2
// ABSTRACT  : This is a callback function for capturing line period
//
// ARGUMENTS : external_irq_callback_args_t * p_args
// RETURN    :
//--------------------------------------------------------------------------------
void irq12_capture_zc2(external_irq_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}

//--------------------------------------------------------------------------------
// NAME      : irq14_capture_zc3
// ABSTRACT  : This is a callback function for capturing line period
//
// ARGUMENTS : external_irq_callback_args_t * p_args
// RETURN    :
//--------------------------------------------------------------------------------
void irq14_capture_zc3(external_irq_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}

//--------------------------------------------------------------------------------
// NAME      : irq15_capture_zc4
// ABSTRACT  : This is a callback function for capturing line period
//
// ARGUMENTS : external_irq_callback_args_t * p_args
// RETURN    :
//--------------------------------------------------------------------------------
void irq15_capture_phase_zc(external_irq_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
