//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: watchdog_driver.c is responsible for initializing the watchdog
//  timer, resarting the watchdog.
//
//-----------------------------------------------------------------------------
#include "watchdog_driver.h"
#include "input_thread.h"

//--------------------------------------------------------------------------------
// NAME      : init_watchdog_driver
// ABSTRACT  : This function initializes the watchdog timer
//
//  see properties for watchdog: Start Watchdog After Configuration = False
//
// ARGUMENTS :
// RETURN    :
//
// Note: the watchdog with the debugger connected by default the timer will not count.
// It can be enabled by clearing the DBGSTOP_WDT bit in the DBGSTOPCR register
//
//
// With PCLKB running at 60 MHz the WDT will reset the device 2.23 seconds after the last refresh.
// WDT clock = 60 MHz / 8192 = 7.32 kHz
// Cycle time = 1 / 7.324 kHz = 136.53 us
// Timeout = 136.53 us x 16384 = 2.23 seconds
//
//--------------------------------------------------------------------------------
void init_watchdog_driver(void)
{
    g_wdt0.p_api->open(g_wdt0.p_ctrl, (wdt_cfg_t *const)g_wdt0.p_cfg);
}

//--------------------------------------------------------------------------------
// NAME      : start_restart_watchdog
// ABSTRACT  : This function restart / refreshes the watchdog timer
//
// ARGUMENTS :
// RETURN    :
//
// Note: watchdog is configured to start when refresh is called
//--------------------------------------------------------------------------------
void start_restart_watchdog(void)
{
    g_wdt0.p_api->refresh(g_wdt0.p_ctrl);
}

//--------------------------------------------------------------------------------
// NAME      : watchdog_callback
// ABSTRACT  : callback function called when NMI occurs
//
// ARGUMENTS : wdt_callback_args_t *p_args
// RETURN    :
//--------------------------------------------------------------------------------
void watchdog_callback(wdt_callback_args_t *p_args)
{
    // watchdog timer expired before refresh
    // reset board, go into initial state here
    SSP_PARAMETER_NOT_USED(p_args);
}
