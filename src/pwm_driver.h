//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: pwm_driver.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __PWM_DRIVER_H__
#define __PWM_DRIVER_H__

void init_pwm_driver(void);


#endif // __PWM_DRIVER_H__
