//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: cli_uart_driver.c is responsible for transmitting / receiving
//  UART bytes.
//
//-----------------------------------------------------------------------------

//    Include Files
//***************************************************************************
#include <stdio.h>
#include <stdarg.h>
#include "cli_uart_driver.h"
#include "cli_thread.h"


//    Constants & Definitions  (File-scope)
//****************************************************************************

//    Structures, enums, etc.  (File-scope)
//****************************************************************************

//    Function prototypes - private members
//****************************************************************************
static uint16_t cli_incRxIndex(uint16_t index);


//    Variable declarations  (File-scope / static)
//****************************************************************************
static cli_uart_rx_t Rx;
static cli_uart_tx_t Tx;


//****************************************************************************
//    Code Starts Here
//****************************************************************************

//--------------------------------------------------------------------------------
// NAME      : init_cli_uart
// ABSTRACT  : This function opens and initializes the CLI serial port.
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void init_cli_uart(void)
{
    // open and initialize the serial port
    cli_uart4.p_api->open(cli_uart4.p_ctrl, cli_uart4.p_cfg);
}


//--------------------------------------------------------------------------------
// NAME      : DebugOutput
// ABSTRACT  : This function opens and initializes the CLI serial port.
// ARGUMENTS : None
// RETURN    : None
//--------------------------------------------------------------------------------
void DebugOutput(const char* szFormat, ...)
{
    va_list args;
    va_start(args, szFormat);
    memset(&Tx.Buffer[0], 0, CLI_BUFSIZE);
    int len = vsnprintf((char*)Tx.Buffer, sizeof(Tx.Buffer), szFormat, args);
    cli_uart_put((const uint8_t *)&Tx.Buffer[0], (uint16_t)len);
    va_end(args);
}


//--------------------------------------------------------------------------------
// NAME      : cli_uart4_callback
// ABSTRACT  : This function is invoked by the UART4 ISR.  It will retrieve a byte
// from the UART.
// ARGUMENTS :
//   uart_callback_args_t - uart event to process
// RETURN    : None
//--------------------------------------------------------------------------------
void cli_uart4_callback(uart_callback_args_t *p_args)
{
    // Get Event Type
    switch (p_args->event)
    {
        // Transmission Complete
        case UART_EVENT_TX_COMPLETE:
            Tx.Complete = true;
            break;
        case UART_EVENT_RX_CHAR:
        case UART_EVENT_RX_COMPLETE:
            cli_uart4.p_api->read(cli_uart4.p_ctrl, (uint8_t const *)&Rx.Buffer[Rx.wIndex], 1);
            Rx.wIndex = cli_incRxIndex(Rx.wIndex);
            break;
        default:
            break;
    }
}


//--------------------------------------------------------------------------------
// NAME      : cli_uart_put
// ABSTRACT  : This function transmits a fixed-length string of data.
// ARGUMENTS :
//   ptr - points to the tx buffer of data to transmit
//   length - specifies the number of bytes to transmit
// RETURN    : None
//--------------------------------------------------------------------------------
void cli_uart_put(const uint8_t *ptr, uint16_t length)
{
    Tx.Ptr=ptr;
    Tx.Length=length;
    Tx.Complete=false;
    cli_uart4.p_api->write(cli_uart4.p_ctrl, (uint8_t const *) (ptr), length);
    while(Tx.Complete==false) {;}
}


//--------------------------------------------------------------------------------
// NAME      : cli_uart_putString
// ABSTRACT  : This function transmits a null-terminated string.
// ARGUMENTS :
//   ptr - points to the tx buffer of the string to transmit
// RETURN    : None
//--------------------------------------------------------------------------------
void cli_uart_putString(const char *ptr)
{
    cli_uart_put((const uint8_t *)ptr, (uint16_t)strlen(ptr));
}


//--------------------------------------------------------------------------------
// NAME      : cli_uart_dataAvailable
// ABSTRACT  : This function informs the caller if data has been received over
// the CLI serial port.
// ARGUMENTS : None
// RETURN    :
//   returns TRUE if at least 1-byte has been rcvd; returns 0 otherwise.
//--------------------------------------------------------------------------------
bool cli_uart_dataAvailable(void)
{
    return (Rx.wIndex != Rx.rIndex);
}


//--------------------------------------------------------------------------------
// NAME      : cli_uart_getByte
// ABSTRACT  : This function returns to the caller the rcvd byte.
// ARGUMENTS : None
// RETURN    :
//   returns the rcvd byte
//--------------------------------------------------------------------------------
uint8_t cli_uart_getByte(void)
{
    uint8_t byte;

    byte = Rx.Buffer[Rx.rIndex];
    Rx.rIndex = cli_incRxIndex(Rx.rIndex);
    return byte;
}


//    Private Members (static)
//******************************************************************************

//--------------------------------------------------------------------------------
// NAME      : cli_incRxIndex
// ABSTRACT  : This function increments (without overruning the buffer) the index.
// ARGUMENTS :
//   index - The index to increment
// RETURN    :
//   returns the incremented index
//--------------------------------------------------------------------------------
static uint16_t cli_incRxIndex(uint16_t index)
{
    if (++index < CLI_BUFSIZE)
    {
        return index;
    }
    return 0;
}
