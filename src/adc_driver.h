//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: adc_driver.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __ADC_DRIVER_H__
#define __ADC_DRIVER_H__

void init_adc_driver(void);


#endif // __ADC_DRIVER_H__
