//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: hw_io_api.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __HW_IO_API_H__
#define __HW_IO_API_H__
#include <stdint.h>
#include "common_def.h"
#include "hal_data.h"

// IDs for the 4 latch-on Relays
typedef enum {
    RELAY_1   = 1,
    RELAY_2   = 2,
    RELAY_3   = 3,
    PHASE_RELAY = 4
} RELAY_ID_E;

// Relay State
typedef enum {
    CLOSE = 1,
    OPEN = 0
} RELAY_STATE_E;

// IDs for the PWM outputs
typedef enum {
    PWM1   = 0,
    PWM2   = 1,
    PWM3   = 2,
    PWM4   = 3
} PWM_ID_E;

// IDs for the Digital Inputs
typedef enum {
    DIGINPUT_1 = 0,
    DIGINPUT_2 = 1,
    DIGINPUT_3 = 2,
    DIGINPUT_4 = 3
}DIGIN_ID_E;

typedef enum {
    LATCH1 = 0,
    LATCH2 = 1,
    LATCH3 = 2
}LATCH_ID_E;

typedef enum {
    LATCH_ENABLE = 0,
    LATCH_DISABLE =1
}LATCH_ENABLE_ID_E;

typedef enum {
    LED_OFF = 0,
    LED_ON = 1
}LED_STATE_ID_E;

// IDs for the 4 Zero-Cross Circuits of the Relays
typedef enum {
    ZC_RELAY_1   = 0,
    ZC_RELAY_2   = 1,
    ZC_RELAY_3   = 2
} ZC_RELAY_ID_E;

typedef enum {
    ZC_ENABLE     = 1,
    ZC_DISABLE    = 0
} ZC_ENABLE_E;

// PWM output channels
#define A_OUTPUT 0
#define B_OUTPUT 1

typedef enum {
    ANALOG_SW_AI3 = 0,
    ANALOG_SW_AI4 = 1
}ANALOG_SW_E;

typedef enum {
    SW_CLOSE = 1,
    SW_OPEN = 0
} AN_SWITCH_STATE_E;

typedef enum {
    PHASE_GATE_ENABLE = 1,
    PHASE_GATE_DISABLE = 0
} PHASE_GATE_E;

typedef struct{
    uint16_t pwr_n;

    uint16_t analog1;
    uint16_t analog2;
    uint16_t analog3;
    uint16_t analog4;

    uint16_t current1;
    uint16_t current2;
    uint16_t current3;
    uint16_t current4;
}adc_channels_t;

STATUS_E hw_io_create_tx_timer(void);
STATUS_E hw_read_adc_channels(adc_channels_t *adc_data_ptr);
void hw_set_zc_enable(ZC_RELAY_ID_E zc_relay_id, ZC_ENABLE_E zc_enable);
void hw_set_led(LATCH_ID_E latch_id, uint8_t  address, LED_STATE_ID_E led_data);
void hw_read_digital_input(DIGIN_ID_E digital_input, ioport_level_t * port_status);
void hw_test(void);
STATUS_E hw_set_pwm_output(PWM_ID_E pwm_id, unsigned pwm_value);
void hw_set_relay(RELAY_ID_E relay_id, RELAY_STATE_E relay_state);
void hw_set_analog_switch(ANALOG_SW_E analog_switch_sel, AN_SWITCH_STATE_E analog_switch_state);
void hw_set_phase_gate_enable(PHASE_GATE_E phase_enable);
#endif // __HW_IO_API_H__
