//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: cli_uart_driver.h contains constants, macros, structure
//  definitions, and public prototypes owned by the CLI driver.
//
//-----------------------------------------------------------------------------
#ifndef __CLI_UART_DRIVER_H__
#define __CLI_UART_DRIVER_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


// Public Definitions and Constants
//******************************************************************************
#define CLI_BUFSIZE  128
#define COUNT(a)      (sizeof(a) / sizeof(a[0]))


// Macros - I/O definitions
//******************************************************************************

// Typedefs - Public structs, enums, etc.
//******************************************************************************
typedef struct {
  uint16_t wIndex;
  uint16_t rIndex;
  uint8_t  Buffer[CLI_BUFSIZE];
} cli_uart_rx_t;

typedef struct {
  uint16_t      Length;
  const uint8_t *Ptr;
  bool          Complete;
  uint8_t       Buffer[CLI_BUFSIZE];
} cli_uart_tx_t;


// Global Variable declarations (extern)
//******************************************************************************

// Prototype Definitions (extern)
//******************************************************************************
void    init_cli_uart(void);
void    DebugOutput(const char* szFormat, ...);
void    cli_uart_put(const uint8_t *ptr, uint16_t length);
void    cli_uart_putString(const char *ptr);
bool    cli_uart_dataAvailable(void);
uint8_t cli_uart_getByte(void);

#endif // __CLI_UART_DRIVER_H__
