//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: common_def.h contains constants, macros, structure definitions,
//               and public prototypes used throughout the project.
//
//-----------------------------------------------------------------------------
#ifndef __COMMON_DEF_H__
#define __COMMON_DEF_H__

typedef enum {

    SUCCESS               =    0x0000,
    FAILURE               =    0x0001          // General Failure, catch-all
} STATUS_E;

#endif // __COMMON_DEF_H__
