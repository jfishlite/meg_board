#include "bt_dtm_uart_driver.h"
#include "bt_thread.h"


void init_bt_dtm_uart(void)
{
    // open and initialize the serial port
    btm_dtm_uart7.p_api->open(btm_dtm_uart7.p_ctrl, btm_dtm_uart7.p_cfg);

}

void bt_dtm_uart_callback(uart_callback_args_t *p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
