#include "rtc_i2c_driver.h"
#include "hal_data.h"

void init_rtc_i2c(void)
{
    rtc_i2c0.p_api->open(rtc_i2c0.p_ctrl, rtc_i2c0.p_cfg);
}

void rtc_callback(i2c_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
