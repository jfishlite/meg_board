//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: The file contains the PWM driver code.
//
//-----------------------------------------------------------------------------
#include "pwm_driver.h"
#include "output_thread.h"

//--------------------------------------------------------------------------------
// NAME      : init_pwm_driver
// ABSTRACT  : Initialization function for PWM
//
// ARGUMENTS :
// RETURN    :
//--------------------------------------------------------------------------------
void init_pwm_driver(void)
{
    g_timer2_pwm_1.p_api->open(g_timer2_pwm_1.p_ctrl, g_timer2_pwm_1.p_cfg);   // initialize pwm1

    g_timer0_pwm_2.p_api->open(g_timer0_pwm_2.p_ctrl, g_timer0_pwm_2.p_cfg);   // initialize pwm2

    g_timer9_pwm_3.p_api->open(g_timer9_pwm_3.p_ctrl, g_timer9_pwm_3.p_cfg);   // initialize pwm3

    g_timer10_pwm_4.p_api->open(g_timer10_pwm_4.p_ctrl, g_timer10_pwm_4.p_cfg); // initialize pwm4

}
//--------------------------------------------------------------------------------
// NAME      : pwm1_callback
// ABSTRACT  : Callback function for PWM1
//
// ARGUMENTS : timer_callback_args_t p_args
// RETURN    :
//--------------------------------------------------------------------------------
void pwm1_callback(timer_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
//--------------------------------------------------------------------------------
// NAME      : pwm2_callback
// ABSTRACT  : Callback function for PWM2
//
// ARGUMENTS : timer_callback_args_t p_args
// RETURN    :
//--------------------------------------------------------------------------------
void pwm2_callback(timer_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
//--------------------------------------------------------------------------------
// NAME      : pwm3_callback
// ABSTRACT  : Callback function for PWM3
//
// ARGUMENTS : timer_callback_args_t p_args
// RETURN    :
//--------------------------------------------------------------------------------
void pwm3_callback(timer_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
//--------------------------------------------------------------------------------
// NAME      : pwm4_callback
// ABSTRACT  : Callback function for PWM4
//
// ARGUMENTS : timer_callback_args_t p_args
// RETURN    :
//--------------------------------------------------------------------------------
void pwm4_callback(timer_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}


