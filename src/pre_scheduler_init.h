//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2018
//-----------------------------------------------------------------------------
//
//  Description: pre_scheduler_init.h contains constants, macros, structure
//  definitions, and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef __PRE_SCHEDULER_INIT_H__
#define __PRE_SCHEDULER_H__


void            tx_application_define_user(void *first_unused_memory);

#endif // __PRE_SCHEDULER_H__
